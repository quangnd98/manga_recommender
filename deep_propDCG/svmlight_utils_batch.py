import re
import sys
import traceback
import numpy as np
import tensorflow as tf

from scipy.sparse import csr_matrix

def load_dataset_pos_neg(file, feature_len, return_line_no=False, return_line_no_neg=False, batch_size=16, take_pre_batch=None):
    gen = file_gen_for_pairwise(file, feature_len, return_line_no, return_line_no_neg)
    types = [tf.float32, tf.float32, tf.int8]
    shapes = [tf.TensorShape([feature_len]), tf.TensorShape([None, feature_len]), tf.TensorShape([None])]
    padded_shapes = [[feature_len],[None,feature_len],[None]]

    if return_line_no:
        types.append(tf.int32)
        shapes.append(tf.TensorShape([1]))
        padded_shapes.append([1])

    if return_line_no_neg:
        types.append(tf.int32)
        shapes.append(tf.TensorShape([None]))
        padded_shapes.append([None])

    dataset = tf.data.Dataset.from_generator(
        gen, tuple(types), tuple(shapes)
    )

    if take_pre_batch is not None:
        dataset = dataset.take(take_pre_batch)
    
    dataset = dataset.padded_batch(
        batch_size, padded_shapes = tuple(padded_shapes)
    )
    # output: (batch_size, feature_len) (batch_size, None, feature_len) (batch_size, None) // (batch_size, 1) // (batch_size, None)

    return dataset

def file_gen_for_pairwise(file, feature_len, return_line_no, return_line_no_neg):
    """Record generator for tf.dataset. Assumed file is sorted by qid
    """

    def gen():
        cur_qid = None
        cur_pos, cur_pos_id, cur_negs, cur_negs_id = None, None, [], []
        with open(file, "r") as fh:
            def update_query_state(_label, _qid, _features, line_no):
                nonlocal cur_qid,cur_pos, cur_pos_id, cur_negs, cur_negs_id 

                assert _qid == cur_qid
                if _label > 0:
                    if cur_pos is not None:
                        raise ValueError(f"Multiple positive labels at qid {_qid}")
                    cur_pos = _features
                    cur_pos_id = line_no
                else:
                    cur_negs.append(_features)
                    cur_negs_id.append(line_no)
            
            def init_query(qid):
                nonlocal cur_qid,cur_pos, cur_pos_id, cur_negs , cur_negs_id

                cur_qid = qid
                cur_pos, cur_pos_id, cur_negs, cur_negs_id = None, None, [], []

            def flush_compose():
                nonlocal cur_qid,cur_pos, cur_pos_id, cur_negs , cur_negs_id
                assert cur_pos is not None
                assert len(cur_negs) > 0
                assert cur_pos_id is not None

                ret = [cur_pos, cur_negs, np.ones(len(cur_negs))]
                if return_line_no:
                    ret.append([cur_pos_id])
                if return_line_no_neg:
                    ret.append(cur_negs_id)

                return ret

            for i, line in enumerate(fh):
                try:
                    _label, _qid, _features = parse_line(line, feature_len)

                    if cur_qid is None:
                        # first line, init first query
                        init_query(_qid)
                        update_query_state(_label, _qid, _features, i)
                    elif cur_qid == _qid:
                        # update current query
                        update_query_state(_label, _qid, _features, i)
                    else: # !=
                        # flush old query
                        yield tuple(flush_compose())
                        # init new query
                        init_query(_qid)
                        update_query_state(_label, _qid, _features, i)   
                except AssertionError:
                    _, _, tb = sys.exc_info()
                    traceback.print_tb(tb) # Fixed format
                    tb_info = traceback.extract_tb(tb)
                    filename, line, func, text = tb_info[-1]
                    raise AssertionError(
                        f'An assertion failed at line {i} of {file}'
                        'line {} in statement {}'.format(line, text)
                    )
            # flush last query  
            try:          
                yield tuple(flush_compose())
            except AssertionError:
                _, _, tb = sys.exc_info()
                traceback.print_tb(tb) # Fixed format
                tb_info = traceback.extract_tb(tb)
                filename, line, func, text = tb_info[-1]
                raise AssertionError(
                    f'An assertion failed at line {i} of {file}.\n'
                    'Line {} in statement {}'.format(line, text)
                )
    return gen

def load_dataset(file, feature_len, batch_size=16, take_pre_batch=None):
    return load_dataset_for_ndcg(file, feature_len, True, batch_size, take_pre_batch)

def load_dataset_for_ndcg(file, feature_len, return_line_no=False, batch_size=16, take_pre_batch=None):
    gen = file_gen_for_ndcg(file, feature_len, return_line_no)
    types = [tf.float32, tf.float32, tf.int8]
    shapes = [tf.TensorShape([None]), tf.TensorShape([None, feature_len]), tf.TensorShape([None])]
    padded_shapes = [[None],[None,feature_len],[None]]

    if return_line_no:
        types.append(tf.int32)
        shapes.append(tf.TensorShape([None]))
        padded_shapes.append([None])

    dataset = tf.data.Dataset.from_generator(
        gen, tuple(types), tuple(shapes)
    )

    if take_pre_batch is not None:
        dataset = dataset.take(take_pre_batch)
    
    dataset = dataset.padded_batch(
        batch_size, padded_shapes = tuple(padded_shapes)
    )
    # output: (batch_size, None)  (batch_size, None, feature_len) (batch_size, None) // (batch_size, None)

    return dataset

def file_gen_for_ndcg(file, feature_len, return_line_no):
    """Record generator for tf.dataset. Assumed file is sorted by qid
    """

    def gen():
        cur_qid = None
        cur_labels, cur_fs, cur_ids = [], [], []
        with open(file, "r") as fh:
            def update_query_state(_label, _qid, _features, line_no):
                nonlocal cur_qid, cur_fs, cur_ids, cur_labels
                cur_labels.append(_label)
                cur_fs.append(_features)
                cur_ids.append(line_no)
            
            def init_query(qid):
                nonlocal cur_qid, cur_fs, cur_ids, cur_labels
                cur_qid = qid
                cur_labels, cur_fs, cur_ids = [], [], []

            def flush_compose():
                nonlocal cur_qid, cur_fs, cur_ids, cur_labels
                assert len(cur_fs) > 0
                assert len(cur_fs) == len(cur_ids) == len(cur_labels)

                ret = [cur_labels, cur_fs, np.ones(len(cur_fs))]
                if return_line_no:
                    ret.append(cur_ids)

                return ret

            for i, line in enumerate(fh):
                try:
                    _label, _qid, _features = parse_line(line, feature_len)

                    if cur_qid is None:
                        # first line, init first query
                        init_query(_qid)
                        update_query_state(_label, _qid, _features, i)
                    elif cur_qid == _qid:
                        # update current query
                        update_query_state(_label, _qid, _features, i)
                    else: # !=
                        # flush old query
                        yield tuple(flush_compose())
                        # init new query
                        init_query(_qid)
                        update_query_state(_label, _qid, _features, i)   
                except AssertionError:
                    _, _, tb = sys.exc_info()
                    traceback.print_tb(tb) # Fixed format
                    tb_info = traceback.extract_tb(tb)
                    filename, line, func, text = tb_info[-1]
                    print(f'An assertion failed at line {i}')
                    print('line {} in statement {}'.format(line, text))
                    print('Skipping')
                    continue
            # flush last query  
            try:          
                yield tuple(flush_compose())
            except AssertionError:
                _, _, tb = sys.exc_info()
                traceback.print_tb(tb) # Fixed format
                tb_info = traceback.extract_tb(tb)
                filename, line, func, text = tb_info[-1]
                print(f'An assertion failed at line {i}')
                print('line {} in statement {}'.format(line, text))
                print('Skipping')
    return gen

qid_re = re.compile("qid:(\d+)")
rem_val =re.compile(":[+-]?[0-9.]+")
rem_id = re.compile("\d+:")
def parse_line(line, feature_len):
    # 0 qid:1 0:0.1538932291451074 1:-0.0304264553959541 2:-0.0495697960433078 3:-0.0796648995023628 4:0.1186061699882002
    columns = line.split(" ")

    # Label
    label = float(columns[0])
    # Qid 
    qid = qid_re.fullmatch(columns[1])
    qid = int(qid.group(1))
    # Features
    feat_ids = []
    feat_vals = []
    for col in columns[2:]:
        i = int(rem_val.sub("", col))
        val = float(rem_id.sub("", col))

        feat_ids.append(i)
        feat_vals.append(val)    
    features = csr_matrix((feat_vals, (np.zeros_like(feat_ids), feat_ids)),shape=(1,feature_len)).toarray().flatten()
    assert features.shape == (feature_len,)

    return label, qid, features

