# The naming is meaning less

A collection of projects revolving around learning from click-through logs

## ./collection_dataset_building

Build svmlight format dataset from collection logs.

Parameters are defined in build_dataset.py::main
Examples are in build_dataset_script.sh

## ./regression_em

Bias propensity estimation from logs by regression_em.

All the test runs, comparisons and comments are in ./20191201_20191231_all.ipynb.
The implementations are in regression_bsed_em_V2* in which one are the main class containing the regression_based em algorithm, others are subclasses of the main one to adapt to different ML backend (test for xgboost and keras for tensorflow (i knr my naming sucks))

## ./deep_propDCG

Unbiased learning to rank optimizing for ndcg

- ./data_transform.ipynb: transform svmlight format to npy format since it is slow handling text directly by keras
- ./train_batch_v2.ipynb: main imple + param tuning

## ./for_collection_dataset.ipynb

Guide to 
- build data
- estimate bias
- deepPropDCG
on collection dataset

## ./ULTRA

- united api for some letor model by and used in https://arxiv.org/pdf/2004.13574.pdf https://github.com/ULTR-Community/ULTRA/
- segregated between 3 layers: input simulation, learning algo (pairwise loss, etc), ranking model (underlying archite)
- main loop: main.py
- conducted experiment: experiments/ experiments/README.md
- implementation: ultra/
- data preparation: data/README.md

## dataset note

### data_bk/cate: 

collection dataset with feature 1 and 2 is the id of user and serie respectively (table user_uid_to_id and serie_mid_to_id) for wide and deep-like learning

| user_joint | serie_joint | user_train | serie_train | q_train | user_test | serie_test | q_test | q_test_missing_from_train |
| ---------- | ----------- | ---------- | ----------- | ------- | --------- | ---------- | ------ | ------------------------- |
| 42241      | 5255        | 88494      | 5585        | 185728  | 58517     | 5364       | 103686 | 24861                     |


