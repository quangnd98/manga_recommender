from functools import partial

import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.python.framework import ops
from tensorflow.python.ops import math_ops

# from regression_based_em import RegressionBasedEM
from regression_based_em_V2 import RegressionBasedEM


def decode_libsvm(line, feature_len, return_label=True):
    line = tf.strings.regex_replace(line, "qid:\d+ ", "", 
                                    replace_global=False,
                                    name="remove_qid"
                                   )
    columns = tf.strings.split([line], " ", name="split_space")

    labels = tf.strings.to_number(columns.values[0], out_type=tf.int32, name="label_to_num")
    labels = tf.reshape(labels, [-1])

    feat_ids = tf.strings.regex_replace(
        columns.values[1:], ":[+-]?[0-9.]+", "",
        replace_global=False,
        name="extract_ids"
    )
    feat_vals = tf.strings.regex_replace(
        columns.values[1:], "\d+:", "",
        replace_global=False,
        name="extract_vals"
    )

    feat_ids = tf.strings.to_number(feat_ids, out_type=tf.int64, name="ids_to_num")
    feat_vals = tf.strings.to_number(feat_vals, out_type=tf.float32, name="vals_to_num")

    feat_ids = tf.reshape(feat_ids, [-1, 1])
    feat_vals = tf.reshape(feat_vals, [-1])

    sparse_feature = tf.SparseTensor(feat_ids, feat_vals, [feature_len])
    dense_feature = tf.sparse.to_dense(sparse_feature)

    if return_label:
        return dense_feature, labels
    else:
        return dense_feature


def load_feature(file_path, feature_len, return_label=True, batch_size=1024):
    _decode_libsvm = partial(
        decode_libsvm, feature_len=feature_len, return_label=return_label,
    )

    return tf.data.TextLineDataset([file_path]).map(_decode_libsvm,
        num_parallel_calls=tf.data.experimental.AUTOTUNE).batch(batch_size)


def set_label_to_feature_dataset(feature_dataset, label, batch_size=1024):
    label_dataset = tf.data.Dataset.from_tensor_slices(label).batch(batch_size)

    return tf.data.Dataset.zip((feature_dataset, label_dataset)).prefetch(tf.data.experimental.AUTOTUNE)


class KerasLearnerWrapper:
    def __init__(self, model, batch_size=None):
        self.model = model
        self.initial_epoch = 0
        self.batch_size = batch_size

    def fit(self, X, y: np.ndarray, verbose=1):
        dataset = set_label_to_feature_dataset(
            load_feature(X, feature_len=feature_len, batch_size=batch_size, return_label=False),
            y,
            self.batch_size
        )

        self.model.fit(
            x=dataset, epochs=self.initial_epoch+1, verbose=verbose, initial_epoch=self.initial_epoch
        )
        self.initial_epoch += 1

    def predict(self, X):
        X = load_feature(X, feature_len=feature_len, batch_size=batch_size, return_label=False)
        
        preds = self.model.predict(X)
        return preds


def create_keras_model(feature_len):
    model = keras.Sequential(
        [
            
            keras.layers.Dense(64, input_shape=[feature_len], activation=tf.nn.relu),
#             keras.layers.Dense(64, activation=tf.nn.relu),
            keras.layers.Dense(1, activation=tf.nn.sigmoid),
        ]
    )
    model.compile(
        optimizer=keras.optimizers.Adam(learning_rate=0.0001), loss=tf.keras.losses.BinaryCrossentropy(), metrics=["AUC"],
    )
    return model


def save_model(model, path):
    model.save(path)
    print(f"Saved model to {path}")


def save_learner(learner, path):
    import joblib

    joblib.dump(learner, path)
    print(f"Saved learnrt to {path}")

if __name__ == "__main__":
    # Setting
    X_columns = np.concatenate(
        [
            [f"user_latent_{i}" for i in range(50)],
            [f"user_history_short_latent_{i}" for i in range(50)],
            [f"user_history_long_latent_{i}" for i in range(50)],
            [f"user_audiences_users_latent_{i}" for i in range(50)],
            [f"user_audiences_genres_latent_{i}" for i in range(50)],
            [f"collection_genres_latent_{i}" for i in range(50)],
            [f"collection_series_latent_{i}" for i in range(50)],
            [f"serie_latent_{i}" for i in range(50)],
            ["user_serie_history"],
        ]
    )
    feature_len = len(X_columns)

    no_examples = 13474846
    batch_size = 1024
    no_batch_per_it = np.ceil(no_examples / batch_size)

    # Load data
    X = '../data_bk/_single_click_20191201_20191231.svmlight'

    X_k = np.load('../data_bk/_single_click_20191201_20191231.X_k.npy', allow_pickle=True) - 1 #min X_k was 1

    X_c = np.load('../data_bk/_single_click_20191201_20191231.X_c.npy', allow_pickle=True)
    X_c = np.clip(X_c, 0, 1)
    
    # Load data
#     X = '../data_bk/_20191201_20191231.svmlight'

#     X_k = np.load('../data_bk/_20191201_20191231.X_k.npy', allow_pickle=True) - 1 #min X_k was 1

#     X_c = np.load('../data_bk/_20191201_20191231.X_c.npy', allow_pickle=True)
#     X_c = np.clip(X_c, 0, 1)

#     X = '../data_bk/_single_click_20200101_20200107.svmlight'
# #     X = f'{Xpath}#{Xpath}.cache'

# #     c_to_gamma = np.array([0.0, 1., 1., 1.])
    c_to_gamma = np.array([0.1, 0.5, 0.7, 0.8])

#     X_k = np.load('../data_bk/_single_click_20200101_20200107.X_k.npy', allow_pickle=True) - 1 #min X_k was 1

#     X_c = np.load('../data_bk/_single_click_20200101_20200107.X_c.npy', allow_pickle=True)
#     X_c = np.clip(X_c, 0, 1)


    # Init data
    theta0 = 1 / (np.arange(X_k.max() + 1) + 1)
    gamma0 = c_to_gamma.take(X_c)
    gamma0 = np.clip(gamma0 + np.random.normal(0, 0.05, gamma0.shape), 0, 1)

    # Init learner
    model = create_keras_model(feature_len)
    learner = KerasLearnerWrapper(model,batch_size)

    # Go!
    reg = RegressionBasedEM(
        learner=learner, max_iter=100, tol=1e-4, n_iter_no_change=5, verbose="debug",
    )

    reg.fit(
        X=X, X_k=X_k, X_c=X_c, gamma_0=gamma0, theta_0=theta0, array_check=False,
    )

    save_model(reg.learner.model, "keras_lr1e-4.h5")
    del reg.learner.model
    save_learner(reg, "learner_keras_lr1e-4.model")
