"""
Aman Agarwal, Xuanhui Wang, Cheng Li, Michael Bendersky, and Marc Najork. 2019. 
Addressing Trust Bias for Unbiased Learning-to-Rank. In The World Wide Web Conference (WWW ’19). 
Association for Computing Machinery, New York, NY, USA, 4–14. 
DOI:https://doi.org/10.1145/3308558.3313697 


TODO:
- Follow https://scikit-learn.org/stable/developers/develop.html 
(parameters checking, flags setting, random state managing...)
"""

import gc
import numpy as np
from scipy.special import expit
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics import log_loss
from sklearn.utils.validation import check_array, check_is_fitted, check_X_y
from xgboost import XGBClassifier, XGBRegressor
from xgboost.sklearn import XGBModel
from sklearn.utils import check_random_state

__all__ = ['TrustPBM']


class TrustPBM(BaseEstimator, RegressorMixin):
    """TrustPBM

    Parameters
    ----------
    max_iter : int, default=200
        Maximum number of iterations. The solver iterates until convergence 
        (determined by ‘tol’) or this number of iterations.
    n_estimators : int, default=100
        The number of boosting stages to perform in underlying gbdt.
    learning_rate : float, optional, default=0.2
        The shirnkage of contribution of each tree in underlying gbdt.
    max_depth : integer, optional, default=3
        maximum depth of the individual regression estimators in underlying gbdt.
    tol : float, default=1e-4
        Tolerance for the optimization. When the loss or score is not improving
        by at least ``tol`` for ``n_iter_no_change`` consecutive iterations,
        convergence is considered to be reached and training stops.
    n_iter_no_change : int, default=10
        Maximum number of epochs to not meet ``tol`` improvement.
    gbdt_lib : str, default='xgb'
        Underlying gbdt library. 'xgb' or 'sklearn'
    n_jobs : int, default=None
        Number of parallel threads used to run xgboost.
    verbose : bool, default=False
        Whether to print progress messages to stdout.
    random_state : RandomState or int, default=None
        A random number generator instance to define the state of the
        random permutations generator.
    """

    def __init__(
        self,
        max_iter=200,
        n_estimators=100,
        learning_rate=0.2,
        max_depth=3,
        tol=1e-4,
        n_iter_no_change=10,
        gbdt_lib='xgb',
        n_jobs=None,
        verbose=False,
        random_state=None,
        gpu=False,
    ):
        self.max_iter = max_iter
        self.n_estimators = n_estimators
        self.learning_rate = learning_rate
        self.max_depth = max_depth
        self.tol = tol
        self.n_iter_no_change = n_iter_no_change
        self.gbdt_lib = gbdt_lib
        self.n_jobs = n_jobs
        self.verbose = verbose
        self.random_state = random_state
        self.gpu = gpu

    def fit(self,
            X,
            X_k,
            X_c,
            gamma_0=None,
            theta_0=None,
            ep_plus_0=None,
            ep_minus_0=None,
            max_rank=None,
            ):
        """Fit the model to data.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            Feature vector x_{q,d}
        X_k : array-like, shape (n_samples,)
            Presented rank for each (q,d) pair in X
            Expected to be in int, either 0-indexed or 1-indexed, spaced by 1
        X_c : array-like, shape (n_samples,)
            Clicked flag for each (q,d) pair in X (1 or 0)
        gamma_0 : array-like, shape (n_samples,)
            Initial value for gamma - P(relavance = 1|q,d)
        theta_0 : array-like, shape (n_ranks,)
            Initial value for theta - P(examined = 1|k).
            Should have all value for all presented rank in X_k
        max_rank : int
            The highest rank aka the first displayed. Usually 1 or 0
            Yeah the naming sucks, cut me some slack

        Returns
        -------
        self : object
            Returns self.
        """
        self.random_state_ = check_random_state(self.random_state)
        self.max_rank_ = max_rank

        if gamma_0 is None:
            gamma_0 = np.clip(X_c, 0.1, 0.9)
            gamma_0 = np.clip(
                gamma_0 + self.random_state_.normal(0, 0.05, gamma_0.shape), 0, 1)

        if self.max_rank_ is None:
            self.max_rank_ = X_c.min()

        if theta_0 is None:
            theta_0 = 1 / np.arange(self.max_rank_, X_k.max()+1)

        if ep_plus_0 is None:
            raise NotImplementedError

        if ep_minus_0 is None:
            raise NotImplementedError

        if len(theta_0) != (X_k.max() - X_k.min() + 1):
            raise ValueError(
                'theta should have all value for all presented rank in X_k')

        X, X_c = check_X_y(X, X_c)
        X, X_k = check_X_y(X, X_k)
        X, gamma_0 = check_X_y(X, gamma_0)

        self.thetas_ = [np.copy(theta_0)]
        # self.gammas_ = [np.copy(gamma_0)]
        self.ep_pluses_ = [np.copy(ep_plus_0)]
        self.ep_minuses_ = [np.copy(ep_minus_0)]
        self.losses_ = []

        if self.gbdt_lib == 'sklearn':
            self.gbdt_ = GradientBoostingRegressor(
                max_depth=self.max_depth,
                learning_rate=self.learning_rate,
                warm_start=True,
                verbose=bool(self.verbose),
                random_state=self.random_state_,
            )
        elif self.gbdt_lib == 'xgb':
            self.gbdt_ = XGBRegressor(
                objective='reg:logistic',
                n_estimators=self.n_estimators,
                max_depth=self.max_depth,
                learning_rate=self.learning_rate,
                n_jobs=self.n_jobs,
                verbosity=1 if bool(self.verbose) else 0,
                random_state=self.random_state_.randint(0, 1000),
                tree_method='gpu_hist' if self.gpu else 'auto'
            )
            # self.gbdt_ = XGBClassifier(
            #     objective='binary:logistic',
            #     n_estimators=self.n_estimators,
            #     max_depth=self.max_depth,
            #     learning_rate=self.learning_rate,
            #     n_jobs=self.n_jobs,
            #     verbosity=1 if bool(self.verbose) else 0
            #     random_state=self.random_state_.randint(0,1000),
            # )
            booster = None
        else:
            raise ValueError(f'Unknown gbdt_lib value: {self.gbdt_lib}')

        gamma = np.copy(gamma_0)
        theta = np.copy(theta_0)
        ep_plus = ep_plus_0
        ep_minus = ep_minus_0
        no_improvement_count = 0

        for it in range(self.max_iter):
            if self.verbose:
                print(f'It {it}:')

            # estimate the hidden variable probability based on eq1
            X_theta = theta.take(X_k-self.max_rank_)
            X_ep_plus = ep_plus.take(X_k-self.max_rank_)
            X_ep_minus = ep_minus.take(X_k-self.max_rank_)

            c1_denominator = X_ep_plus * gamma + X_ep_minus * (1-gamma)
            c1_denominator = np.where(
                c1_denominator == 0, 1e-15, c1_denominator)
                
            c0_denominator = (1 - X_theta * c1_denominator)
            c0_denominator = np.where(
                c0_denominator == 0, 1e-15, c0_denominator)

            pe0r1_c1 = np.zeros((X.shape[0]))  # (examples, )
            # pe0r0_c1 = np.zeros((X.shape[0]))  # (examples, )
            pe1r1_c1 = (X_ep_plus * gamma) / c1_denominator
            pe1r0_c1 = (X_ep_minus * (1 - gamma)) / c1_denominator

            pe0r1_c0 = ((1-X_theta)*gamma) / c0_denominator
            # pe0r0_c0 = ((1-X_theta)*(1-gamma)) / c0_denominator
            pe1r1_c0 = (X_theta*(1-X_ep_plus)*gamma) / c0_denominator
            pe1r0_c0 = (X_theta*(1-X_ep_minus)*(1-gamma)) / c0_denominator

            # marginals
            pe1_c0 = pe1r0_c0 + pe1r1_c0
            pr1_c0 = pe0r1_c0 + pe1r1_c0
            pr1_c1 = pe0r1_c1 + pe1r1_c1

            del pe0r1_c1
            del pe0r1_c0

            pr1 = X_c * pr1_c1 + (1-X_c) * pr1_c0

            del pr1_c0
            del pr1_c1

            # sample r from pr1
            S_label = np.where(pr1 >= 0.5, 1, 0)
            # if self.verbose: print(f'S_label true% {S_label.sum() / len(S_label)}:')

            # GBDT fitting
            if self.verbose == 'debug':
                print(f'Fitting gbdt...')

            if isinstance(self.gbdt_, GradientBoostingRegressor):
                self.gbdt_.fit(X, S_label)
            elif isinstance(self.gbdt_, XGBModel):
                self.gbdt_.fit(X, S_label, xgb_model=booster)
                booster = self.gbdt_.get_booster()
            else:
                raise ValueError(f'Unknown gbdt type: {type(self.gbdt)}')

            # theta, ep_plus, ep_minus update
            if self.verbose == 'debug':
                print(f'Updating theta, ep_plus, ep_minus...')
            theta_range = range(len(theta))

            for k in theta_range:
                k_mask = ((X_k-self.max_rank_) == k)

                theta[k] = (k_mask * (X_c + (1 - X_c) * pe1_c0)
                            ).sum() / k_mask.sum()
                if np.isnan(theta[k]):
                    theta[k] = theta[k-1] if k > 0 else 1

                ep_plus[k] = (k_mask * X_c * pe1r1_c1).sum() / \
                    (k_mask * (X_c * pe1r1_c1 + (1-X_c) * pe1r1_c0)).sum()
                if np.isnan(ep_plus[k]):
                    ep_plus[k] = ep_plus[k-1] if k > 0 else 1

                ep_minus[k] = (k_mask * X_c * pe1r0_c1).sum() / \
                    (k_mask * (X_c * pe1r0_c1 + (1-X_c) * pe1r0_c0)).sum()
                if np.isnan(ep_minus[k]):
                    ep_minus[k] = ep_minus[k-1] if k > 0 else 1
            # theta = np.clip(theta, 0, 1)
            # ep_plus = np.clip(ep_plus, 0, 1)
            # ep_minus = np.clip(ep_minus, 0, 1)

            self.thetas_.append(np.copy(theta))
            self.ep_pluses_.append(np.copy(ep_plus))
            self.ep_minuses_.append(np.copy(ep_minus))

            # gamma update based on eq 3
            if self.verbose == 'debug':
                print(f'Updating gamma...')
            S_predict = self.gbdt_.predict(X)
            gamma = np.clip(S_predict, 1e-15, 1-1e-15)
            # gamma = expit(S_predict)

            # self.gammas_.append(np.copy(gamma))

            # converge test
            loss = log_loss(S_label, gamma)
            if self.verbose:
                print(f'Loss: {loss}')

            if len(self.losses_) > 1 and loss > min(self.losses_) - self.tol:
                no_improvement_count += 1
            else:
                no_improvement_count = 0
            self.losses_.append(loss)
            if self.verbose == 'debug':
                print(f'no_improvement_count = {no_improvement_count}')

            if no_improvement_count > self.n_iter_no_change:
                print(
                    f"Validation score did not improve more than tol={self.tol} for {self.n_iter_no_change} consecutive epochs.")
                break
            gc.collect()

        self.is_fitted_ = True
        return self

    def predict(self, X):
        """Predict using the model.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            Feature vector x_{q,d}

        Returns
        -------
        y : ndarray, shape (n_samples,)
        """
        X = check_array(X, accept_sparse=True)
        check_is_fitted(self, 'is_fitted_')

        y = self.gbdt_.predict(X)
        return y


if __name__ == "__main__":
    pass
    # import pandas as pd
    # from sklearn.metrics import confusion_matrix

    # df = pd.read_pickle('data/_raw_20191201_20191207.pkl')
    # df['serie_order_line'] = (df.serie_order - 1)//3 + 1

    # X_columns = np.concatenate([
    #     [f'user_latent_{i}' for i in range(50)],
    #     [f'user_history_short_latent_{i}' for i in range(50)],
    #     [f'user_history_long_latent_{i}' for i in range(50)],
    #     [f'user_audiences_users_latent_{i}' for i in range(50)],
    #     [f'user_audiences_genres_latent_{i}' for i in range(50)],
    #     [f'collection_genres_latent_{i}' for i in range(50)],
    #     [f'collection_series_latent_{i}' for i in range(50)],
    #     [f'serie_latent_{i}' for i in range(50)],
    #     ['user_serie_history']
    # ])
    # X = df[X_columns]
    # X = X.fillna(0).values

    # c_to_gamma = np.array([0.05, 0.1, 0.7, 0.8])

    # X_k = df.serie_order_line.values
    # X_c = (df.label.values > 0).astype(int)

    # theta0 = 1/np.arange(1, X_k.max()+1)
    # gamma0 = c_to_gamma.take(df.label.values)

    # del df

    # reg = RegressionBasedEM(
    #     max_iter=10,
    #     n_estimators=100,
    #     n_jobs=10,
    #     n_iter_no_change=3,
    #     verbose=True
    # )

    # reg.fit(
    #     X=X,
    #     X_k=X_k,
    #     X_c=X_c,
    #     gamma_0=gamma0,
    #     theta_0=theta0,
    #     max_rank=1,
    # )

    # preds = reg.predict(X)
    # print(log_loss(X_c, preds))
    # print(confusion_matrix(X_c, preds))
