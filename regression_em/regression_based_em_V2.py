"""
Regression-based EM
Xuanhui Wang, Nadav Golbandi, Michael Bendersky, Donald Metzler, and Marc Najork. 2018. 
Position Bias Estimation for Unbiased Learning to Rank in Personal Search. 
In Proceedings of the Eleventh ACM International Conference on Web Search and Data Mining (WSDM ’18). 
Association for Computing Machinery, New York, NY, USA, 610–618. 
DOI:https://doi.org/10.1145/3159652.3159732
"""

import warnings
import numpy as np
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.metrics import log_loss
from sklearn.utils import check_random_state
from sklearn.utils.validation import check_array, check_is_fitted, check_X_y

__all__ = ["RegressionBasedEM"]


class RegressionBasedEM(BaseEstimator, RegressorMixin):
    """Regression-based EM

    Parameters
    ----------
    learner: obj
        The learner must implement following methods:
            - learner.fit(X, y): Fit to (X, y) in an updating manner.
                Equivalent to run an epoch.
            - learner.predict(X): Predict for X.
                Preferred to use sigmoid as activation
    max_iter : int, default=200
        Maximum number of iterations. 
        The solver would iterates until convergence 
            (determined by ‘tol’) or this number of iterations.
    tol : float, default=1e-4
        Tolerance for the optimization. 
        When the loss or score is not improving by at least
            ``tol`` for ``n_iter_no_change`` consecutive iterations,
            convergence is considered to be reached and training stops.
    n_iter_no_change : int, default=10
        Maximum number of epochs to not meet ``tol`` improvement.
    verbose : bool or "debug", default=False
        Whether to print progress messages to stdout.
    random_state : RandomState or int, default=None
        A random number generator instance to define
            the state of the random permutations generator.
    """

    def __init__(
        self,
        learner,
        max_iter=200,
        tol=1e-4,
        n_iter_no_change=10,
        verbose=False,
        random_state=None,
    ):
        self.learner = learner
        self.max_iter = max_iter
        self.tol = tol
        self.n_iter_no_change = n_iter_no_change
        self.verbose = verbose
        self.random_state = random_state

    def fit(
        self, X, X_k, X_c=None, gamma_0=None, theta_0=None, array_check=True,
    ):
        """Fit the model to data.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            Feature vector x_{q,d}
            Can be other suitable input to learner.
                e.g: DMatrix for xgboost
                If this is the case, consider turning off array_check
        X_k : array-like, shape (n_samples,)
            Presented rank for each (q,d) pair in X
            Assumed all value to be 1-separated int
                with the highest rank (first displayed) is 0
                i.e. n_ranks = max(X_k) + 1
        X_c : array-like, shape (n_samples,)
            Clicked flag for each (q,d) pair in X (1 or 0)
        gamma_0 : array-like, shape (n_samples,)
            Initial value for gamma - P(relevance = 1|q,d)
        theta_0 : array-like, shape (n_ranks,)
            Initial value for theta - P(examined = 1|k).
        array_check: bool
            Perform input shape checks. 
            Only makes sense if all the parameters are array-like

        Returns
        -------
        self : object
            Returns self.
        """
        self.random_state_ = check_random_state(self.random_state)
        self.no_improvement_count_ = 0

        if gamma_0 is None:
            gamma_0 = np.clip(X_c, 0.1, 0.9)
            gamma_0 = np.clip(
                gamma_0 + self.random_state_.normal(0, 0.05, gamma_0.shape), 0, 1
            )

        if theta_0 is None:
            theta_0 = 1 / (np.arange(X_k.max() + 1) + 1)

        if len(theta_0) != X_k.max() + 1:
            raise ValueError(
                "theta should have all value for all presented rank in X_k"
            )

        if min(X_k) != 0:
            warnings.warn("min(X_k) != 0")

        if array_check:
            X, X_c = check_X_y(X, X_c)
            X, X_k = check_X_y(X, X_k)
            X, gamma_0 = check_X_y(X, gamma_0)

        self.thetas_ = [np.copy(theta_0)]
        self.losses_ = []

        gamma = np.copy(gamma_0)
        theta = np.copy(theta_0)

        try:
            for it in range(self.max_iter):
                if self.verbose:
                    print(f"It {it}:")

                if self.verbose == "debug":
                    print("Expectation step...")

                # estimate the hidden variable probability based on eq1
                X_theta = theta.take(X_k)
                
                denominator = 1 - X_theta * gamma
                denominator = np.where(denominator == 0, 1e-15, denominator)

                pe1r1_c1 = np.ones(len(X_c))  # (examples, )
                pe1r0_c0 = (X_theta * (1 - gamma)) / denominator  # (examples, )
                pe0r1_c0 = ((1 - X_theta) * gamma) / denominator  # (examples, )
                pe0r0_c0 = ((1 - X_theta) * (1 - gamma)) / denominator  # (examples, )
                pe1r1_c0 = np.clip(1 - pe1r0_c0 - pe0r1_c0 - pe0r0_c0, 0.0, 1.0)
#                 pe1r1_c0 = 1 - pe1r0_c0 - pe0r1_c0 - pe0r0_c0

                # marginals
                pe1 = X_c * pe1r1_c1 + (1 - X_c) * (pe1r1_c0 + pe1r0_c0)
                pr1 = X_c * pe1r1_c1 + (1 - X_c) * (pe1r1_c0 + pe0r1_c0)
                
                del pe1r1_c1
                del pe1r0_c0
                del pe0r1_c0
                del pe0r0_c0
                del pe1r1_c0

                # sample r from pr1
                S_label = np.where(pr1 >= 0.5, 1, 0)

                if self.verbose == "debug":
                    print(f"Maximization step ...")

                self.learner.fit(X, S_label)

                # theta update based on eq 2
                if self.verbose == "debug":
                    print(f"Updating theta...")

                theta_range = range(len(theta))
                for k in theta_range:
                    theta[k] = ((X_k == k) * (X_c + (1 - X_c) * pe1)).sum() / (
                        X_k == k
                    ).sum()
                    theta[k] = np.clip(theta[k], 0.0, 1.0)
                    if np.isnan(theta[k]):
                        theta[k] = theta[k - 1] if k > 0 else 1
                self.thetas_.append(np.copy(theta))

                # gamma update based on eq 3
                if self.verbose == "debug":
                    print(f"Updating gamma...")
                S_predict = self.learner.predict(X)
                if S_predict.ndim > 1:
                    S_predict = S_predict.flatten()
                gamma = np.clip(S_predict, 1e-15, 1-1e-5)
#                 gamma = S_predict

                # converge test
                loss = log_loss(S_label, gamma)
                
                import pickle
                pickle.dump((S_label, gamma), open('debug.pkl', 'wb'))
                
                if self.verbose:
                    print(f"Loss: {loss}")

                if self._early_stop(loss):
                    print(
                        f"Validation score did not improve more than tol={self.tol} "
                        f"for {self.n_iter_no_change} consecutive epochs."
                    )
                    break

        except (MemoryError, KeyboardInterrupt) as err:
            print(err)
            print(err.args)

        self.is_fitted_ = True
        return self

    def predict(self, X, array_check=True):
        """Predict using the model.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            Feature vector x_{q,d}

        Returns
        -------
        y : ndarray, shape (n_samples,)
        """
        if array_check:
            X = check_array(X, accept_sparse=True)
        check_is_fitted(self, "is_fitted_")

        y = self.learner.predict(X)
        return y

    def _early_stop(self, loss):
        if len(self.losses_) > 1 and loss > min(self.losses_) - self.tol:
            self.no_improvement_count_ += 1
        else:
            self.no_improvement_count_ = 0
        self.losses_.append(loss)
        if self.verbose == "debug":
            print(f"no_improvement_count = {self.no_improvement_count_}")

        if self.no_improvement_count_ > self.n_iter_no_change:
            return True
        else:
            return False


if __name__ == "__main__":
    pass
