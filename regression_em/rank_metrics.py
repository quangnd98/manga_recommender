"""Metrics to assess performance on classification task given scores

Functions named as ``*_score`` return a scalar value to maximize: the higher
the better

Function named as ``*_error`` or ``*_loss`` return a scalar value to minimize:
the lower the better
"""

# Authors: Alexandre Gramfort <alexandre.gramfort@inria.fr>
#          Mathieu Blondel <mathieu@mblondel.org>
#          Olivier Grisel <olivier.grisel@ensta.org>
#          Arnaud Joly <a.joly@ulg.ac.be>
#          Jochen Wersdorfer <jochen@wersdoerfer.de>
#          Lars Buitinck
#          Joel Nothman <joel.nothman@gmail.com>
#          Noel Dawe <noel@dawe.me>
# License: BSD 3 clause


import numpy as np
from joblib import Parallel, delayed
from sklearn.metrics import average_precision_score
from sklearn.utils import check_array, check_consistent_length
from sklearn.utils.multiclass import type_of_target

__all__ = ['mean_average_precision_score', 'ndcg_score']

### MAP ################################################################################

def _ap(y, preds, q, qid):
    """Calculate AP of a query specified by q
    """
    mask = np.equal(qid, q)
    pred_q = preds[mask]
    y_q = y[mask]

    average_precision = average_precision_score(y_q, pred_q)
    return average_precision  # can be nan


def mean_average_precision_score(y, preds, qid, n_jobs=-4, verbose=0):
    sub_scores = Parallel(n_jobs=n_jobs, verbose=verbose)(delayed(_ap)(y, preds, q, qid)
                                                          for q in range(qid.min(), qid.max()+1))
    sub_scores_cumsum = np.nansum(sub_scores)
    sub_scores_na = np.isnan(sub_scores).sum()
    score = sub_scores_cumsum / (len(sub_scores) - sub_scores_na)

    return score

### NDCG ###############################################################################

def _dcg_sample_scores(y_true, y_score, propensity=None,
                       k=None, log_base=2, ignore_ties=False):
    """Compute Discounted Cumulative Gain.

    Sum the true scores ranked in the order induced by the predicted scores,
    after applying a logarithmic discount.

    This ranking metric yields a high value if true labels are ranked high by
    ``y_score``.

    Parameters
    ----------
    y_true : ndarray, shape (n_samples, n_labels)
        True targets of multilabel classification, or true scores of entities
        to be ranked.

    y_score : ndarray, shape (n_samples, n_labels)
        Target scores, can either be probability estimates, confidence values,
        or non-thresholded measure of decisions (as returned by
        "decision_function" on some classifiers).

    k : int, optional (default=None)
        Only consider the highest k scores in the ranking. If None, use all
        outputs.

    log_base : float, optional (default=2)
        Base of the logarithm used for the discount. A low value means a
        sharper discount (top results are more important).

    ignore_ties : bool, optional (default=False)
        Assume that there are no ties in y_score (which is likely to be the
        case if y_score is continuous) for efficiency gains.

    Returns
    -------
    discounted_cumulative_gain : ndarray, shape (n_samples,)
        The DCG score for each sample.

    See also
    --------
    ndcg_score :
        The Discounted Cumulative Gain divided by the Ideal Discounted
        Cumulative Gain (the DCG obtained for a perfect ranking), in order to
        have a score between 0 and 1.

    """
    discount = 1 / (np.log(np.arange(y_true.shape[1]) + 2) / np.log(log_base))
    if propensity is not None:
        discount /=  propensity[:discount.shape[0]]
    if k is not None:
        discount[k:] = 0
    if ignore_ties or True:
        ranking = np.argsort(y_score)[:, ::-1]
        ranked = y_true[np.arange(ranking.shape[0])[:, np.newaxis], ranking]
        cumulative_gains = discount.dot(ranked.T)
    else:
        discount_cumsum = np.cumsum(discount)
        cumulative_gains = [_tie_averaged_dcg(y_t, y_s, discount_cumsum)
                            for y_t, y_s in zip(y_true, y_score)]
        cumulative_gains = np.asarray(cumulative_gains)
    return cumulative_gains


def _tie_averaged_dcg(y_true, y_score, discount_cumsum):
    """
    Compute DCG by averaging over possible permutations of ties.

    The gain (`y_true`) of an index falling inside a tied group (in the order
    induced by `y_score`) is replaced by the average gain within this group.
    The discounted gain for a tied group is then the average `y_true` within
    this group times the sum of discounts of the corresponding ranks.

    This amounts to averaging scores for all possible orderings of the tied
    groups.

    (note in the case of dcg@k the discount is 0 after index k)

    Parameters
    ----------
    y_true : ndarray
        The true relevance scores

    y_score : ndarray
        Predicted scores

    discount_cumsum : ndarray
        Precomputed cumulative sum of the discounts.

    Returns
    -------
    The discounted cumulative gain.

    References
    ----------
    McSherry, F., & Najork, M. (2008, March). Computing information retrieval
    performance measures efficiently in the presence of tied scores. In
    European conference on information retrieval (pp. 414-421). Springer,
    Berlin, Heidelberg.

    """
    _, inv, counts = np.unique(
        - y_score, return_inverse=True, return_counts=True)
    ranked = np.zeros(len(counts))
    np.add.at(ranked, inv, y_true)
    ranked /= counts
    groups = np.cumsum(counts) - 1
    discount_sums = np.empty(len(counts))
    discount_sums[0] = discount_cumsum[groups[0]]
    discount_sums[1:] = np.diff(discount_cumsum[groups])
    return (ranked * discount_sums).sum()


def _check_dcg_target_type(y_true):
    y_type = type_of_target(y_true)
    supported_fmt = ("multilabel-indicator", "continuous-multioutput",
                     "multiclass-multioutput", "binary")
    if y_type not in supported_fmt:
        raise ValueError(
            "Only {} formats are supported. Got {} instead".format(
                supported_fmt, y_type))


def _dcg_score(y_true, y_score, propensity=None, k=None,
               log_base=2, sample_weight=None, ignore_ties=False):
    """Compute Discounted Cumulative Gain.

    Sum the true scores ranked in the order induced by the predicted scores,
    after applying a logarithmic discount.

    This ranking metric yields a high value if true labels are ranked high by
    ``y_score``.

    Usually the Normalized Discounted Cumulative Gain (NDCG, computed by
    ndcg_score) is preferred.

    Parameters
    ----------
    y_true : ndarray, shape (n_samples, n_labels)
        True targets of multilabel classification, or true scores of entities
        to be ranked.

    y_score : ndarray, shape (n_samples, n_labels)
        Target scores, can either be probability estimates, confidence values,
        or non-thresholded measure of decisions (as returned by
        "decision_function" on some classifiers).

    k : int, optional (default=None)
        Only consider the highest k scores in the ranking. If None, use all
        outputs.

    log_base : float, optional (default=2)
        Base of the logarithm used for the discount. A low value means a
        sharper discount (top results are more important).

    sample_weight : ndarray, shape (n_samples,), optional (default=None)
        Sample weights. If None, all samples are given the same weight.

    ignore_ties : bool, optional (default=False)
        Assume that there are no ties in y_score (which is likely to be the
        case if y_score is continuous) for efficiency gains.

    Returns
    -------
    discounted_cumulative_gain : float
        The averaged sample DCG scores.

    See also
    --------
    ndcg_score :
        The Discounted Cumulative Gain divided by the Ideal Discounted
        Cumulative Gain (the DCG obtained for a perfect ranking), in order to
        have a score between 0 and 1.

    References
    ----------
    `Wikipedia entry for Discounted Cumulative Gain
    <https://en.wikipedia.org/wiki/Discounted_cumulative_gain>`_

    Jarvelin, K., & Kekalainen, J. (2002).
    Cumulated gain-based evaluation of IR techniques. ACM Transactions on
    Information Systems (TOIS), 20(4), 422-446.

    Wang, Y., Wang, L., Li, Y., He, D., Chen, W., & Liu, T. Y. (2013, May).
    A theoretical analysis of NDCG ranking measures. In Proceedings of the 26th
    Annual Conference on Learning Theory (COLT 2013)

    McSherry, F., & Najork, M. (2008, March). Computing information retrieval
    performance measures efficiently in the presence of tied scores. In
    European conference on information retrieval (pp. 414-421). Springer,
    Berlin, Heidelberg.

    Examples
    --------
    >>> from sklearn.metrics import dcg_score
    >>> # we have groud-truth relevance of some answers to a query:
    >>> true_relevance = np.asarray([[10, 0, 0, 1, 5]])
    >>> # we predict scores for the answers
    >>> scores = np.asarray([[.1, .2, .3, 4, 70]])
    >>> dcg_score(true_relevance, scores) # doctest: +ELLIPSIS
    9.49...
    >>> # we can set k to truncate the sum; only top k answers contribute
    >>> dcg_score(true_relevance, scores, k=2) # doctest: +ELLIPSIS
    5.63...
    >>> # now we have some ties in our prediction
    >>> scores = np.asarray([[1, 0, 0, 0, 1]])
    >>> # by default ties are averaged, so here we get the average true
    >>> # relevance of our top predictions: (10 + 5) / 2 = 7.5
    >>> dcg_score(true_relevance, scores, k=1) # doctest: +ELLIPSIS
    7.5
    >>> # we can choose to ignore ties for faster results, but only
    >>> # if we know there aren't ties in our scores, otherwise we get
    >>> # wrong results:
    >>> dcg_score(true_relevance,
    ...           scores, k=1, ignore_ties=True) # doctest: +ELLIPSIS
    5.0

    """
    y_true = check_array(y_true, ensure_2d=False)
    y_score = check_array(y_score, ensure_2d=False)
    check_consistent_length(y_true, y_score, sample_weight)
    # _check_dcg_target_type(y_true)
    return np.average(
        _dcg_sample_scores(
            y_true, y_score, propensity=propensity, 
            k=k, log_base=log_base, ignore_ties=ignore_ties),
            weights=sample_weight)


def _ndcg_sample_scores(y_true, y_score, propensity=None, k=None, ignore_ties=False):
    """Compute Normalized Discounted Cumulative Gain.

    Sum the true scores ranked in the order induced by the predicted scores,
    after applying a logarithmic discount. Then divide by the best possible
    score (Ideal DCG, obtained for a perfect ranking) to obtain a score between
    0 and 1.

    This ranking metric yields a high value if true labels are ranked high by
    ``y_score``.

    Parameters
    ----------
    y_true : ndarray, shape (n_samples, n_labels)
        True targets of multilabel classification, or true scores of entities
        to be ranked.

    y_score : ndarray, shape (n_samples, n_labels)
        Target scores, can either be probability estimates, confidence values,
        or non-thresholded measure of decisions (as returned by
        "decision_function" on some classifiers).

    k : int, optional (default=None)
        Only consider the highest k scores in the ranking. If None, use all
        outputs.

    ignore_ties : bool, optional (default=False)
        Assume that there are no ties in y_score (which is likely to be the
        case if y_score is continuous) for efficiency gains.

    Returns
    -------
    normalized_discounted_cumulative_gain : ndarray, shape (n_samples,)
        The NDCG score for each sample (float in [0., 1.]).

    See also
    --------
    dcg_score : Discounted Cumulative Gain (not normalized).

    """
    gain = _dcg_sample_scores(y_true, y_score, propensity, k, ignore_ties=ignore_ties)
    # Here we use the order induced by y_true so we can ignore ties since
    # the gain associated to tied indices is the same (permuting ties doesn't
    # change the value of the re-ordered y_true)
    normalizing_gain = _dcg_sample_scores(y_true, y_true, propensity, k, ignore_ties=True)
    all_irrelevant = normalizing_gain == 0
    gain[all_irrelevant] = 0
    gain[~all_irrelevant] /= normalizing_gain[~all_irrelevant]
    return gain


def __ndcg_score(y_true, y_score, propensity=None, k=None, sample_weight=None, ignore_ties=False):
    """Compute Normalized Discounted Cumulative Gain.

    Sum the true scores ranked in the order induced by the predicted scores,
    after applying a logarithmic discount. Then divide by the best possible
    score (Ideal DCG, obtained for a perfect ranking) to obtain a score between
    0 and 1.

    This ranking metric yields a high value if true labels are ranked high by
    ``y_score``.

    Parameters
    ----------
    y_true : ndarray, shape (n_samples, n_labels)
        True targets of multilabel classification, or true scores of entities
        to be ranked.

    y_score : ndarray, shape (n_samples, n_labels)
        Target scores, can either be probability estimates, confidence values,
        or non-thresholded measure of decisions (as returned by
        "decision_function" on some classifiers).

    k : int, optional (default=None)
        Only consider the highest k scores in the ranking. If None, use all
        outputs.

    sample_weight : ndarray, shape (n_samples,), optional (default=None)
        Sample weights. If None, all samples are given the same weight.

    ignore_ties : bool, optional (default=False)
        Assume that there are no ties in y_score (which is likely to be the
        case if y_score is continuous) for efficiency gains.

    Returns
    -------
    normalized_discounted_cumulative_gain : float in [0., 1.]
        The averaged NDCG scores for all samples.

    See also
    --------
    dcg_score : Discounted Cumulative Gain (not normalized).

    References
    ----------
    `Wikipedia entry for Discounted Cumulative Gain
    <https://en.wikipedia.org/wiki/Discounted_cumulative_gain>`_

    Jarvelin, K., & Kekalainen, J. (2002).
    Cumulated gain-based evaluation of IR techniques. ACM Transactions on
    Information Systems (TOIS), 20(4), 422-446.

    Wang, Y., Wang, L., Li, Y., He, D., Chen, W., & Liu, T. Y. (2013, May).
    A theoretical analysis of NDCG ranking measures. In Proceedings of the 26th
    Annual Conference on Learning Theory (COLT 2013)

    McSherry, F., & Najork, M. (2008, March). Computing information retrieval
    performance measures efficiently in the presence of tied scores. In
    European conference on information retrieval (pp. 414-421). Springer,
    Berlin, Heidelberg.

    Examples
    --------
    >>> from sklearn.metrics import ndcg_score
    >>> # we have groud-truth relevance of some answers to a query:
    >>> true_relevance = np.asarray([[10, 0, 0, 1, 5]])
    >>> # we predict some scores (relevance) for the answers
    >>> scores = np.asarray([[.1, .2, .3, 4, 70]])
    >>> ndcg_score(true_relevance, scores) # doctest: +ELLIPSIS
    0.69...
    >>> scores = np.asarray([[.05, 1.1, 1., .5, .0]])
    >>> ndcg_score(true_relevance, scores) # doctest: +ELLIPSIS
    0.49...
    >>> # we can set k to truncate the sum; only top k answers contribute.
    >>> ndcg_score(true_relevance, scores, k=4) # doctest: +ELLIPSIS
    0.35...
    >>> # the normalization takes k into account so a perfect answer
    >>> # would still get 1.0
    >>> ndcg_score(true_relevance, true_relevance, k=4) # doctest: +ELLIPSIS
    1.0
    >>> # now we have some ties in our prediction
    >>> scores = np.asarray([[1, 0, 0, 0, 1]])
    >>> # by default ties are averaged, so here we get the average (normalized)
    >>> # true relevance of our top predictions: (10 / 10 + 5 / 10) / 2 = .75
    >>> ndcg_score(true_relevance, scores, k=1) # doctest: +ELLIPSIS
    0.75
    >>> # we can choose to ignore ties for faster results, but only
    >>> # if we know there aren't ties in our scores, otherwise we get
    >>> # wrong results:
    >>> ndcg_score(true_relevance,
    ...           scores, k=1, ignore_ties=True) # doctest: +ELLIPSIS
    0.5

    """
    y_true = check_array(y_true, ensure_2d=False)
    y_score = check_array(y_score, ensure_2d=False)
    check_consistent_length(y_true, y_score, sample_weight)
    _check_dcg_target_type(y_true)
    gain = _ndcg_sample_scores(y_true, y_score, propensity=propensity, k=k, ignore_ties=ignore_ties)
    return np.average(gain, weights=sample_weight)


def _ndcg_score(y, preds, q, qid, propensity):
    """Calculate NDCG of a query specified by q
    """
    mask = np.equal(qid, q)
    pred_q = preds[mask]
    y_q = y[mask]

    try:
        score = __ndcg_score([y_q], [pred_q], propensity)
    except ValueError:
        return np.nan
    return score  # can be nan


def ndcg_score(y, preds, qid, propensity=None, n_jobs=-4, verbose=0):
    sub_scores = Parallel(n_jobs=n_jobs, verbose=verbose)(delayed(_ndcg_score)(y, preds, q, qid, propensity)
                                                          for q in range(qid.min(), qid.max()+1))
    sub_scores_cumsum = np.nansum(sub_scores)
    sub_scores_na = np.isnan(sub_scores).sum()
    score = sub_scores_cumsum / (len(sub_scores) - sub_scores_na)

    return score

### Weight MRR #########################################################################

def _w_mrr_score(y, preds, theta, q, qid):
    """Calculate weight mrr of a query specified by q
    """
    mask = np.equal(qid, q)
    pred_q = preds[mask]
    y_q = y[mask]

    if sum(y_q > 0) != 1:
        raise ValueError(
            "Only one true document allowed for each query. "
            f"Query {q} has {sum(y_q > 0)} positive labels."
        )
    
    true_index_in_y = y_q.argmax()
    pred_of_true = pred_q[true_index_in_y]

    rank_i = np.searchsorted(np.sort(-pred_q), -pred_of_true)+1
    w_i = theta[rank_i]

    return w_i / rank_i, w_i

def w_mrr_score(y, preds, theta, qid, n_jobs=-4, verbose=0):
    ret = Parallel(n_jobs=n_jobs, verbose=verbose)(delayed(_w_mrr_score)(y, preds, theta, q, qid)
                                                   for q in np.unique(qid))

    sub_scores, w_is = list(zip(*ret))
    sub_scores_cumsum = np.nansum(sub_scores)

    w_with_non_na_score = (w_is * ~np.isnan(sub_scores)).sum()

    return sub_scores_cumsum / w_with_non_na_score

if __name__ == "__main__":

    # assert(w_mrr_score(
    #     y = np.array([[1,0,0],[0,1,0],[0,0,1]]).flatten(),
    #     preds = np.array([[1,0,0],[0,1,0],[0,0,1]]).flatten(),
    #     theta = [np.nan, 1, 0.5, 1/3.0,],
    #     qid = np.array([[1,1,1],[2,2,2],[3,3,3]]).flatten(),
    # ) == 1.0)

    # assert(w_mrr_score(
    #     y = np.array([[1,0,0],[0,1,0],[0,0,1]]).flatten(),
    #     preds = np.array([[0.5,.3,.45],[.6,.8,.1],[0.2,.3,1]]).flatten(),
    #     theta = [np.nan, 1, 0.5, 1/3.0,],
    #     qid = np.array([[1,1,1],[2,2,2],[3,3,3]]).flatten(),
    # ) == 1.0)

    # TODO
    # assert(w_mrr_score(
    #     y = np.array([[1,0,0],[0,1,0],[0,0,1]]).flatten(),
    #     preds = np.array([[1,0,0],[1,0,0.5],[0,0,1]]).flatten(),
    #     theta = [np.nan, 1, 0.5, 1/3.0,],
    #     qid = np.array([[1,1,1],[2,2,2],[3,3,3]]).flatten(),
    # ))

    # assert(w_mrr_score(
    #     y = np.array([[0,0,1],[0,1,0],[1,0,0]]).flatten(),
    #     preds = np.array([[3,2,1],[3,2,1],[3,2,1]]).flatten(),
    #     theta = [np.nan, 1, 1,1],
    #     qid = np.array([[1,1,1],[2,2,2],[3,3,3]]).flatten(),
    # ) == 0.6111111111111111)

    assert(round(ndcg_score(
        y = np.array([3,2,3,0,1,2]),
        preds = np.array([6,5,4,3,2,1]),
        qid = np.array([1,1,1,1,1,1]),
    ),4) == 0.9608)


    # true_relevance = np.asarray([[10, 0, 0, 1, 5]])
    # scores = np.asarray([[.1, .2, .3, 4, 70]])
    # qid = np.asarray([[1, 1, 1, 1, 1]])
    # assert(
    #     round(ndcg_score(true_relevance, scores, qid),5) == 0.69569
    # )
    # # 0.69...
    
    # scores = np.asarray([[.05, 1.1, 1., .5, .0]])
    # assert(
    #     round(ndcg_score(true_relevance, scores, qid),2) == 0.49
    # )
    # # 0.49..

    print()

    print(round(ndcg_score(
        y = np.array([3,2,3,0,1,2]),
        preds = np.array([6,5,4,3,2,1]),
        qid = np.array([1,1,1,1,1,1]),
        propensity = np.array(1/np.arange(1,10))
    ),4))