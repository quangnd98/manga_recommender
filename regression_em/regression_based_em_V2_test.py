import numpy as np
import pandas as pd
import seaborn as sns
import xgboost as xgb
from sklearn.metrics import log_loss, confusion_matrix
# from regression_based_em import RegressionBasedEM
from regression_based_em_V2 import RegressionBasedEM

class XGBoostLearnerWrapper():
    
    def __init__(
        self, params,
    ):
        self.params = params
        self.model = None

    def fit(self, X, y):
        X.set_label(y)
        self.model = xgb.train(
            params=self.params, dtrain=X, num_boost_round=5, xgb_model=self.model
        )

    def predict(self, X):
        return self.model.predict(X)

if __name__ == "__main__":

    X_columns = np.concatenate([
        [f'user_latent_{i}' for i in range(50)],
        [f'user_history_short_latent_{i}' for i in range(50)],
        [f'user_history_long_latent_{i}' for i in range(50)],
        [f'user_audiences_users_latent_{i}' for i in range(50)],
        [f'user_audiences_genres_latent_{i}' for i in range(50)],
        [f'collection_genres_latent_{i}' for i in range(50)],
        [f'collection_series_latent_{i}' for i in range(50)],
        [f'serie_latent_{i}' for i in range(50)],
        ['user_serie_history']
    ])
    
    Xpath = '../data_bk/_single_click_20191201_20191231.svmlight'
    X = f'{Xpath}#{Xpath}.cache'

    c_to_gamma = np.array([0.1, 0.5, 0.7, 0.8])

    X_k = np.load('../data_bk/_single_click_20191201_20191231.X_k.npy', allow_pickle=True) - 1 #min X_k was 1
    X_c = np.load('../data_bk/_single_click_20191201_20191231.X_c.npy', allow_pickle=True)
    X_c = np.clip(X_c, 0, 1)


    theta0 = 1 / (np.arange(X_k.max()+1)+1)
    theta0 = np.zeros_like(theta0)

    gamma0 = c_to_gamma.take(X_c)
#     gamma0 = np.clip(gamma0 + np.random.normal(0, 0.05, gamma0.shape), 0, 1)

    xgb_params = {
        'objective': 'reg:logistic',
        'max_depth': 3,
        'learning_rate': 0.2,
        'verbosity': 1,
        'tree_method': 'gpu_hist'
    }

    learner = XGBoostLearnerWrapper(xgb_params)
    X = xgb.DMatrix(X)

    reg = RegressionBasedEM(
        learner=learner,
        max_iter=100,
        tol=1e-4,
        n_iter_no_change=5,
        verbose='debug',
    )

    reg.fit(
        X=X,
        X_k=X_k,
        X_c=X_c,
        gamma_0=gamma0,
        theta_0=theta0,
        array_check=False,
    )

    import joblib
    filename = 'V2_5.model'
    joblib.dump(reg, filename)
    print(f'Model saved to {filename}')