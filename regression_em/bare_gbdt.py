#!/usr/bin/env python

import numpy as np
from scipy.special import expit
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics import log_loss
from sklearn.utils.validation import check_array, check_is_fitted, check_X_y
from xgboost import XGBClassifier, XGBRegressor, XGBRanker
from xgboost.sklearn import XGBModel

if __name__ == "__main__":

    import pandas as pd

    print('Load data')
    df = pd.read_pickle('data/_raw_20191201_20191207.pkl')
    df['serie_order_line'] = (df.serie_order - 1)//3 + 1

    print('Sort data')
    df['qid'] = df.groupby(['user_id', 'session_start', 'collection']).ngroup()
    df = df.sort_values('qid', ascending=True)
    qid = df.groupby(['qid']).ngroup().value_counts(sort=False).sort_index().values

    print('Compose data')
    X_columns = np.concatenate([
        [f'user_latent_{i}' for i in range(50)],
        [f'user_history_short_latent_{i}' for i in range(50)],
        [f'user_history_long_latent_{i}' for i in range(50)],
        [f'user_audiences_users_latent_{i}' for i in range(50)],
        [f'user_audiences_genres_latent_{i}' for i in range(50)],
        [f'collection_genres_latent_{i}' for i in range(50)],
        [f'collection_series_latent_{i}' for i in range(50)],
        [f'serie_latent_{i}' for i in range(50)],
        ['user_serie_history']
    ])
    X = df[X_columns]
    X = X.fillna(0).values

    c_to_gamma = np.array([0.05, 0.1, 0.7, 0.8])

    X_k = df.serie_order_line.values
    X_c = (df.label.values > 0).astype(int)

    theta0 = 1/np.arange(1, X_k.max()+1)
    gamma0 = c_to_gamma.take(df.label.values)

    del df

    xgb = XGBRanker(
        n_jobs=10,
        verbosity=1
    )

    print('Fit data')
    xgb.fit(X, X_c, qid)

    print('Predict data')
    preds = xgb.predict(X)

    loss = log_loss(X_c, preds)

    print(loss)
