from __future__ import print_function
from __future__ import absolute_import
import os
import sys
import itertools
import numpy as np
import tensorflow as tf
from ultra.ranking_model import BaseRankingModel
import ultra

from deepctr.models import DeepFM as _DeepFM
from deepctr.inputs import DenseFeat


class DeepFMWithEmbeddingsLearning(BaseRankingModel):
    """DeepFM with the ability to learn embedding for categoricals fields.

    Categorical fields, their corresponding number of labels must be predefined and
    properly encoded (see sklearn.preprocessing.LabelEncoder)
    """

    def __init__(self, hparams_str):
        """Create the network.

        Args:
            hparams_str: (String) The hyper-parameters used to build the network.
        """

        self.hparams = ultra.utils.hparams.HParams(
            task="binary",
            hidden_layer_sizes=[128, 128],
            activation_func="relu",
            initializer="None",
            categorical_indice_list=[-1],  # 0-indexed, svmlight must index feature from 1
            categorical_no_labels_list=[-1],
            categorical_constant_initializer_path_list=["None"],
            embedding_dim=10,
        )
        self.hparams.parse(hparams_str)
        self.model_parameters = {}
        self.act_func = None
        self.initializer = None
        if self.hparams.initializer in BaseRankingModel.INITIALIZER_DIC:
            self.initializer = BaseRankingModel.INITIALIZER_DIC[
                self.hparams.initializer
            ]
        if self.hparams.activation_func in BaseRankingModel.ACT_FUNC_DIC:
            self.act_func = BaseRankingModel.ACT_FUNC_DIC[self.hparams.activation_func]

        self.categorical_indice_list = (
            []
            if self.hparams.categorical_indice_list[0] == -1
            else self.hparams.categorical_indice_list
        )
        self.categorical_no_labels_list = (
            []
            if self.hparams.categorical_no_labels_list[0] == -1
            else self.hparams.categorical_no_labels_list
        )
        self.categorical_initializer_list = []
        for p, _ in itertools.zip_longest(
            self.hparams.categorical_constant_initializer_path_list,
            self.categorical_indice_list
        ):
            try:
                if os.path.isfile(p):
                    self.categorical_initializer_list.append(np.load(p, allow_pickle=True))
                else:
                    self.categorical_initializer_list.append(None)
            except:
                self.categorical_initializer_list.append(None)

        assert len(self.categorical_indice_list) == len(self.categorical_no_labels_list)
        assert len(self.categorical_indice_list) == len(self.categorical_initializer_list)
        self.embedding_layers = [
            tf.keras.layers.Embedding(
                no_label, 
                self.hparams.embedding_dim, 
                embeddings_initializer=init,
                input_length=1, 
                name=f"emb_id_{idx}"
            )
            for idx,no_label,init in zip(
                self.categorical_indice_list, 
                self.categorical_no_labels_list,
                self.categorical_initializer_list
            )
        ]

        self.model_parameters = {}

    def build(
        self,
        input_list,
        noisy_params=None,
        noise_rate=0.05,
        is_training=False,
        **kwargs,
    ):
        """ Create the model

        Args:
            input_list: (list<tf.tensor>) A list of tensors containing the features
                        for a list of documents.
            noisy_params: (dict<parameter_name, tf.variable>) A dictionary of noisy parameters to add.
            noise_rate: (float) A value specify how much noise to add.
            is_training: (bool) A flag indicating whether the model is running in training mode.

        Returns:
            A list of tf.Tensor containing the ranking scores for each instance in input_list.
        """
        with tf.variable_scope(
            tf.get_variable_scope(), initializer=self.initializer, reuse=tf.AUTO_REUSE
        ):
            input_data = tf.concat(input_list, axis=0)  # None, F
            number_features = input_data.get_shape()[-1].value

            # ---------- Dense features ----------
            self.dense_indice_list = [
                i
                for i in range(number_features)
                if i not in self.categorical_indice_list
            ]
            if len(self.dense_indice_list) != 0:
                dense_input = [tf.gather(
                    input_data, self.dense_indice_list, axis=1
                )]  # None, no_dense
            else:
                dense_input = []

            # ---------- Embeddings --------------
            categorical_embeddings = []  # list of None, emb_dim
            for idx, embedding_layer in zip(
                self.categorical_indice_list, self.embedding_layers
            ):
                categorical_labels = tf.gather(input_data, [idx], axis=1)
                embeddings = embedding_layer(categorical_labels)
                embeddings = tf.reshape(embeddings, shape=(-1, self.hparams.embedding_dim))
                categorical_embeddings.append(embeddings)

            # ---------- Final embedding output --------------
            input_data = tf.concat(
                categorical_embeddings + dense_input, axis=1
            )  # None, F
            number_features = (
                len(self.dense_indice_list)
                + len(self.categorical_indice_list) * self.hparams.embedding_dim
            )

            # ---------- FM componenet ----------
            self.y_first_order = tf.reduce_sum(input_data, -1, keepdims=True)  # None, 1
            self.y_first_order = tf.keras.layers.LayerNormalization()(
                self.y_first_order
            )

            squared_sum = tf.square(tf.reduce_sum(input_data, axis=-1))
            sum_squared = tf.reduce_sum(tf.square(input_data), axis=-1)
            self.y_second_order = tf.reshape((squared_sum - sum_squared) / 2.0, (-1, 1))
            self.y_second_order = tf.keras.layers.LayerNormalization()(
                self.y_second_order
            )

            # ---------- Deep component ----------
            # TODO: layer norm and dropout
            self.y_deep = input_data
            current_size = number_features
            for i, layer_size in enumerate(self.hparams.hidden_layer_sizes):
                weight = self.get_variable(
                    f"W_{i}",
                    (current_size, layer_size),
                    noisy_params=noisy_params,
                    noise_rate=noise_rate,
                )
                bias = self.get_variable(
                    f"b_{i}",
                    (layer_size),
                    noisy_params=noisy_params,
                    noise_rate=noise_rate,
                )

                self.y_deep = self.act_func(tf.matmul(self.y_deep, weight) + bias)
                self.y_deep = tf.keras.layers.LayerNormalization()(self.y_deep)
                current_size = layer_size

            # ---------- DeepFM ----------
            # concat_input = tf.concat([self.y_fm, self.y_deep], axis=1)
            concat_input = tf.concat(
                [self.y_first_order, self.y_second_order, self.y_deep], axis=1
            )
            # current_size = 1 + number_features*number_features + current_size
            current_size = 1 + 1 + current_size
            weight = self.get_variable(
                f"W_pred",
                (current_size, 1),
                noisy_params=noisy_params,
                noise_rate=noise_rate,
            )
            bias = self.get_variable(
                f"b_pred", (1), noisy_params=noisy_params, noise_rate=noise_rate
            )
            self.out = tf.add(tf.matmul(concat_input, weight), bias)
            self.out = tf.math.sigmoid(self.out)

            return tf.split(self.out, len(input_list), axis=0)
