from __future__ import print_function
from __future__ import absolute_import
import os
import sys
import tensorflow as tf
from ultra.ranking_model import BaseRankingModel
import ultra

from deepctr.models import DeepFM as _DeepFM
from deepctr.inputs import DenseFeat

class DeepFM(BaseRankingModel):
    """DeepFM but only for dense features.
    Only contains from and must be fed with the embeddings layer.
    """

    def __init__(self, hparams_str):
        """Create the network.

        Args:
            hparams_str: (String) The hyper-parameters used to build the network.
        """

        self.hparams = ultra.utils.hparams.HParams(
            task="binary",
            hidden_layer_sizes=[128,128],
            activation_func='relu',
            initializer='None',
        )
        self.hparams.parse(hparams_str)
        self.model_parameters = {}
        self.act_func = None
        self.initializer = None
        if self.hparams.initializer in BaseRankingModel.INITIALIZER_DIC:
            self.initializer = BaseRankingModel.INITIALIZER_DIC[self.hparams.initializer]
        if self.hparams.activation_func in BaseRankingModel.ACT_FUNC_DIC:
            self.act_func = BaseRankingModel.ACT_FUNC_DIC[self.hparams.activation_func]

        self.model_parameters = {}

    def build(self, input_list, noisy_params=None,
              noise_rate=0.05, is_training=False, **kwargs):
        """ Create the model

        Args:
            input_list: (list<tf.tensor>) A list of tensors containing the features
                        for a list of documents.
            noisy_params: (dict<parameter_name, tf.variable>) A dictionary of noisy parameters to add.
            noise_rate: (float) A value specify how much noise to add.
            is_training: (bool) A flag indicating whether the model is running in training mode.

        Returns:
            A list of tf.Tensor containing the ranking scores for each instance in input_list.
        """
        with tf.variable_scope(tf.get_variable_scope(), initializer=self.initializer, reuse=tf.AUTO_REUSE):
            input_data = tf.concat(input_list, axis=0) # None * F
            number_features = input_data.get_shape()[-1].value

            # ---------- FM componenet ----------
            self.y_first_order = tf.reduce_sum(input_data, -1, keepdims=True)  # None * 1
            self.y_first_order = tf.keras.layers.LayerNormalization()(self.y_first_order)

            squared_sum = tf.square(tf.reduce_sum(input_data, axis=-1))
            sum_squared = tf.reduce_sum(tf.square(input_data), axis=-1)
            self.y_second_order = tf.reshape((squared_sum - sum_squared)/2., (-1, 1))
            self.y_second_order = tf.keras.layers.LayerNormalization()(self.y_second_order)

            # ---------- Deep component ----------
            # TODO: layer norm and dropout
            self.y_deep = input_data
            current_size = number_features
            for i, layer_size in enumerate(self.hparams.hidden_layer_sizes):
                weight = self.get_variable(f"W_{i}", (current_size, layer_size), noisy_params=noisy_params, noise_rate=noise_rate)
                bias = self.get_variable(f'b_{i}', (layer_size), noisy_params=noisy_params, noise_rate=noise_rate)

                self.y_deep = self.act_func(tf.matmul(self.y_deep, weight) + bias)
                self.y_deep = tf.keras.layers.LayerNormalization()(self.y_deep)
                current_size = layer_size

            # ---------- DeepFM ----------
            # concat_input = tf.concat([self.y_fm, self.y_deep], axis=1)
            concat_input = tf.concat([self.y_first_order, self.y_second_order, self.y_deep], axis=1)
            # current_size = 1 + number_features*number_features + current_size
            current_size = 1 + 1 + current_size
            weight = self.get_variable(f"W_pred", (current_size, 1), noisy_params=noisy_params, noise_rate=noise_rate)
            bias = self.get_variable(f'b_pred', (1), noisy_params=noisy_params, noise_rate=noise_rate)
            self.out = tf.add(tf.matmul(concat_input, weight),bias)
            self.out = tf.math.sigmoid(self.out)

            return tf.split(self.out, len(input_list), axis=0)
