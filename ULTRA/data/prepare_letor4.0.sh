Data_path="./LETOR_processed"   ## Data path where to unzip the data
Feature_number=46              ## how many features for LETOR data
Prepro_fun=""                ## additional function to do preprocessing, available, "log", "None", we default normalize data to -1 and 1. If choosing log, it will first using log function to the data and then normalize it to -1 and 1. 

# Download place MQ200x in /data

mkdir LETOR_raw/
unrar x MQ2007.rar LETOR_raw/
unrar x MQ2008.rar LETOR_raw/

# Download SVMrank.
if [ ! -f "svm_rank_classify" ]
then 
    echo "Download svmrank"
    wget http://download.joachims.org/svm_rank/current/svm_rank_linux64.tar.gz
    tar xvzf svm_rank_linux64.tar.gz
fi


# For each fold, merge train08 and train 07, keep 08 as vali and test
mkdir $Data_path
for i in {1..5}; do
    echo "Process fold "$i;
    mkdir $Data_path/Fold$i
    mkdir $Data_path/Fold$i/cleaned_data  # path to store data after cleaning
    mkdir $Data_path/Fold$i/normalized    # path to store data after nomalization
    mkdir $Data_path/Fold$i/tmp_toy       # path to store toy version of training data which is 1% of total dataset
    mkdir $Data_path/Fold$i/tmp_toy/data 
    mkdir $Data_path/Fold$i/tmp_toy/tmp
    mkdir $Data_path/Fold$i/tmp_toy/tmp_data_toy

    # Prepare the dataset.
    # Sort features, sort query id, remove duplicates, and remove queries without relevant documents in validation and test set.
    fold_name=$Data_path/Fold$i
    cat LETOR_raw/MQ2008/Fold$i/train.txt LETOR_raw/MQ2007/Fold$i/train.txt > $fold_name/train.txt
    cp LETOR_raw/MQ2008/Fold$i/test.txt $fold_name
    cp LETOR_raw/MQ2008/Fold$i/vali.txt $fold_name


    echo "begin cleaning"
    python ../libsvm_tools/clean_libsvm_file.py $fold_name/train.txt $fold_name/cleaned_data/train.txt 0
    python ../libsvm_tools/clean_libsvm_file.py $fold_name/vali.txt $fold_name/cleaned_data/valid.txt 1
    python ../libsvm_tools/clean_libsvm_file.py $fold_name/test.txt $fold_name/cleaned_data/test.txt 1
    # Normalize the data
    # Extract the feature statistics for later normalization.
    echo "extract statistics for normalization"
    python ../libsvm_tools/extrac_feature_statistics.py $fold_name/cleaned_data/
    # Normalize the data.
    echo "begin normalization"
    python ../libsvm_tools/normalize_feature.py $fold_name/cleaned_data/feature_scale.json  $fold_name/cleaned_data/test.txt $fold_name/normalized/test.txt $Prepro_fun
    python ../libsvm_tools/normalize_feature.py $fold_name/cleaned_data/feature_scale.json  $fold_name/cleaned_data/train.txt $fold_name/normalized/train.txt $Prepro_fun
    python ../libsvm_tools/normalize_feature.py $fold_name/cleaned_data/feature_scale.json  $fold_name/cleaned_data/valid.txt $fold_name/normalized/valid.txt $Prepro_fun
    # Sample 1% training data to build the initial ranker.
    echo "sample 0.01 for intiial ranker"
    python ../libsvm_tools/sample_libsvm_data.py $fold_name/normalized/train.txt $fold_name/normalized/sampled_train.txt 0.01

    # Conduct initial ranking with SVMrank.
    python ../libsvm_tools/initial_ranking_with_svm_rank.py \
        ./ \
        $fold_name/normalized/sampled_train.txt \
        $fold_name/normalized/valid.txt \
        $fold_name/normalized/test.txt \
        $fold_name/tmp/
    ./svm_rank_classify $fold_name/normalized/train.txt $fold_name/tmp/model.dat $fold_name/tmp/train.predict

    # Prepare model input.
    python ../libsvm_tools/prepare_exp_data_with_svmrank.py $fold_name/normalized/ $fold_name/tmp/ $fold_name/tmp_data/ $Feature_number

    cp $fold_name/normalized/sampled_train.txt $fold_name/tmp_toy/data/train.txt
    cp $fold_name/normalized/sampled_train.txt $fold_name/tmp_toy/data/valid.txt
    cp $fold_name/normalized/sampled_train.txt $fold_name/tmp_toy/data/test.txt
    ./svm_rank_classify $fold_name/tmp_toy/data/train.txt $fold_name/tmp/model.dat $fold_name/tmp_toy/tmp/train.predict
    ./svm_rank_classify $fold_name/tmp_toy/data/valid.txt $fold_name/tmp/model.dat $fold_name/tmp_toy/tmp/valid.predict
    ./svm_rank_classify $fold_name/tmp_toy/data/test.txt $fold_name/tmp/model.dat $fold_name/tmp_toy/tmp/test.predict
    python ../libsvm_tools/prepare_exp_data_with_svmrank.py $fold_name/tmp_toy/data/ $fold_name/tmp_toy/tmp/ $fold_name/tmp_toy/tmp_data_toy/ $Feature_number
done

#  export SETTING_ARGS="--data_dir=$Data_path/tmp_data/ --model_dir=$Data_path/tmp_model/ --output_dir=$Data_path/tmp_output/ --setting_file=./example/offline_setting/ipw_rank_exp_settings.json"
# echo $SETTING_ARGS
# # Run model
# python main.py --max_train_iteration=1000 $SETTING_ARGS

# # Test model
# python main.py --test_only=True $SETTING_ARGS

