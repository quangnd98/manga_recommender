Thisdir=$PWD
cd ../../

OLDIFS=$IFS; 
IFS=','; 

arr=(
    # "offline_DeepPropDCG,example/offline_setting/deep_prop_dcg_exp_settings.json"
    "online_DeepPropDCG,example/online_setting/deep_prop_dcg_exp_settings.json"
)
# Data_dir="data/Yahoo_processed/tmp_data"
Data_dir="data/LETOR_processed/Fold1/tmp_data"

for i in "${arr[@]}"; do 
    set -- $i;
    Label=$1;
    Settingfile=$2;

    echo Run $Label with setting $Settingfile
    mkdir $Thisdir/$Label

    # Run model
    python main.py --max_train_iteration=2000 --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile

    # Test model
    python main.py --test_only=True --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile
done;
IFS=$OLDIFS