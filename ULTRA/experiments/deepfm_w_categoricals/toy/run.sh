Thisdir=$PWD
cd ../../../

OLDIFS=$IFS; 
IFS=','; 

arr=(
    "toy_test,${Thisdir}/toy.json"
)
Data_dir=$Thisdir

for i in "${arr[@]}"; do 
    set -- $i;
    Label=$1;
    Settingfile=$2;

    echo Run $Label with setting $Settingfile

    if [[ -d $Thisdir/$Label ]]; then
        rm -r $Thisdir/$Label
    fi

    mkdir $Thisdir/$Label

    # Run model
    python main.py --max_train_iteration=100 --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile --train_data_prefix=toy --valid_data_prefix=toy

    # Test model
    python main.py --test_only=True --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile --test_data_prefix=toy
done;
IFS=$OLDIFS
