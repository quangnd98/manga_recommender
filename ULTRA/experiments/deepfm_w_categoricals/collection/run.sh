Thisdir=$PWD
cd ../../../

OLDIFS=$IFS; 
IFS=','; 

arr=(
    # "full,${Thisdir}/deepfm_categorical_01.json,settings.json"

    # "no_categorical_deepfm,${Thisdir}/deepfm_categorical_none.json,settings_no_categorical.json"
    # "no_categorical_dnn,${Thisdir}/dnn.json,settings_no_categorical.json"

    "categorical_only_deepfm,${Thisdir}/deepfm_categorical_01.json,settings_categorical_only.json"
    # "categorical_only_dnn,${Thisdir}/dnn.json,settings_categorical_only.json"

    "full_categorical_initialized,${Thisdir}/deepfm_categorical_01_inited.json,settings.json"
    "categorical_only_deepfm_initialized,${Thisdir}/deepfm_categorical_01_inited.json,settings_categorical_only.json"
)
Data_dir="/home/nabdev/quang/manga_recommender/data_bk/cate"

for i in "${arr[@]}"; do 
    set -- $i;
    Label=$1;
    Settingfile=$2;
    DataSettingfile=$3

    echo Run $Label with setting $Settingfile

    if [[ -d $Thisdir/$Label ]]; then
        rm -r $Thisdir/$Label
        # continue
    fi

    mkdir $Thisdir/$Label

    # Run model
    python main.py --max_train_iteration=5000 --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile --train_data_prefix=_20191225_20191231.svmlight --valid_data_prefix=_20200101_20200103.svmlight --data_setting_file_name=$DataSettingfile

    # Test model
    python main.py --test_only=True --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile --test_data_prefix=_20200103_20200105.svmlight --data_setting_file_name=$DataSettingfile
done;
IFS=$OLDIFS
