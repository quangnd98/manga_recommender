Thisdir=$PWD
cd ../../../

OLDIFS=$IFS; 
IFS=','; 

arr=(
    "offline_IPW,${Thisdir}/settings/offline_ipw_rank_exp_settings.json"
    "offline_REM,${Thisdir}/settings/offline_regression_EM_exp_settings.json"
    "offline_DLA_exp,${Thisdir}/settings/offline_dla_exp_settings.json"
    "offline_PDGD,${Thisdir}/settings/offline_pdgd_exp_settings.json"
    "offline_NA,${Thisdir}/settings/offline_naive_algorithm_exp_settings.json"

    # "online_IPW,${Thisdir}/settings/online_ipw_rank_exp_settings.json"
    # "online_REM,${Thisdir}/settings/online_regression_EM_exp_settings.json"
    # "online_DLA_exp,${Thisdir}/settings/online_dla_exp_settings.json"
    # "online_PDGD,${Thisdir}/settings/online_pdgd_exp_settings.json"
    # "online_NA,${Thisdir}/settings/online_naive_algorithm_exp_settings.json"
)
Data_dir="data/Yahoo_processed/tmp_data"
# Data_dir="data/LETOR_processed/Fold1/tmp_data"

for i in "${arr[@]}"; do 
    set -- $i;
    Label=$1;
    Settingfile=$2;

    echo Run $Label with setting $Settingfile
    mkdir $Thisdir/$Label

    # Run model
    python main.py --max_train_iteration=5000 --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile

    # Test model
    python main.py --test_only=True --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile
done;
IFS=$OLDIFS;