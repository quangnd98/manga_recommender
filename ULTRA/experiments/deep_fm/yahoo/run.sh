Thisdir=$PWD
cd ../../../

OLDIFS=$IFS; 
IFS=','; 

arr=(
    #"with_ipw_10k,${Thisdir}/with_ipw.json"
    #"with_dla_10k,${Thisdir}/with_dla.json"
    #"with_na_10k,${Thisdir}/with_na.json"
    "with_deeppropdcg_10k,${Thisdir}/with_deeppropdcg.json"


    #"without_ipw_10k,${Thisdir}/without_ipw.json"
    #"without_dla_10k,${Thisdir}/without_dla.json"
    #"without_na_10k,${Thisdir}/without_na.json"
    #"without_deeppropdcg_10k,${Thisdir}/without_deeppropdcg.json"
)
Data_dir="data/Yahoo_processed/tmp_data"
# Data_dir="data/LETOR_processed/Fold1/tmp_data"

for i in "${arr[@]}"; do 
    set -- $i;
    Label=$1;
    Settingfile=$2;

    echo Run $Label with setting $Settingfile

    if [[ -d $Thisdir/$Label ]]; then
        rm -r $Thisdir/$Label
    fi

    mkdir $Thisdir/$Label

    # Run model
    python main.py --max_train_iteration=10000 --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile 

    # Test model
    python main.py --test_only=True --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile 
done;
IFS=$OLDIFS
