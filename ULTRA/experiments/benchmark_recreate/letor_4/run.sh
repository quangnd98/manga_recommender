Thisdir=$PWD
cd ../../

OLDIFS=$IFS; 
IFS=','; 

arr=(
    "offline_IPW,example/offline_setting/ipw_rank_exp_settings.json"
    # "offline_REM,example/offline_setting/regression_EM_exp_settings.json"
    # "offline_DLA_DLCM,example/offline_setting/dla_DLCM_exp_settings.json"
    # "offline_DLA_exp,example/offline_setting/dla_exp_settings.json"
    # "offline_PairD,example/offline_setting/pairwise_debias_exp_settings.json"
    # "offline_DBGD,example/offline_setting/dbgd_exp_settings.json"
    # "offline_PDGD,example/offline_setting/pdgd_exp_settings.json"
    # "offline_NA,example/offline_setting/naive_algorithm_exp_settings.json"

    # "online_IPW,example/online_setting/ipw_rank_exp_settings.json"
    # "online_REM,example/online_setting/regression_EM_exp_settings.json"
    # "online_DLA_DLCM,example/online_setting/dla_DLCM_exp_settings.json"
    # "online_DLA_exp,example/online_setting/dla_exp_settings.json"
    # "online_PairD,example/online_setting/pairwise_debias_exp_settings.json"
    # "online_DBGD,example/online_setting/dbgd_exp_settings.json"
    # "online_PDGD,example/online_setting/pdgd_exp_settings.json"
    # "online_NA,example/online_setting/naive_algorithm_exp_settings.json"
)
# Data_dir="data/Yahoo_processed/tmp_data"
Data_dir="data/LETOR_processed/Fold1/tmp_data"

for i in "${arr[@]}"; do 
    set -- $i;
    Label=$1;
    Settingfile=$2;

    echo Run $Label with setting $Settingfile
    mkdir $Thisdir/$Label

    # Run model
    python main.py --max_train_iteration=2000 --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile

    # Test model
    python main.py --test_only=True --data_dir=$Data_dir/ --model_dir=$Thisdir/$Label/tmp_model/ --output_dir=$Thisdir/$Label/tmp_output/ --setting_file=$Settingfile
done;
IFS=$OLDIFS