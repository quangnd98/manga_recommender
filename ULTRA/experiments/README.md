# Experiments:

Benchmark_recreate: Reconduct the experiment in https://arxiv.org/pdf/2004.13574.pdf as sanity check  
original_vs_np: refactor to use numpy ndarray instead of list for memory efficiency  
deep_prop_dcg: implementation of deep_prop_dcg  

run with run.sh in each folder, make sure the ```cd``` command cd to the ULTRA folder

## baseline on letor 4.0

on 2000 interations

| ndcg@10                | off    | on (ons?) |
| ---------------------- | ------ | --------- |
| IPW                    | 0.6835 | 0.7158    |
| REM                    | 0.6391 | 0.5829    |
| DLA                    | 0.6087 |           |
| PairD                  | 0.5098 | 0.5172    |
| DBGD                   | 0.5336 | 0.4346    |
| PDGD                   | 0.3795 | 0.7202    |
| NA                     | 0.6486 | 0.7215    |
| Deep_PropDCG           | 0.6884 | 0.6994    |
| Deep_PropDCG  + deepfm | 0.4555 |           | flat eval ndcg? deepfm model didnt learn? |

## recreate on yahoo

### dataset
- train: 19133 queries, 139 list size
- valid 2869 queries, 117 list size
- test 6717 qeuroes, 129 list size
- 700 features

### original setup
- original results are run multiples times with the same param and reported with the best chosen by validation set
- validate set a chosen with real label truth
- originals trained on 10k iterations

### run 1

- 5k iteration, 1 run/each
- validation values are against real label

train: simluated clicks; valid: real label

----------|original result|recreated results|
| ndcg@10 | off   | ons   | off    | ons    |
| ------- | ----- | ----- | ------ | ------ |
| IPW     | 0.755 | 0.753 | 0.757  | 0.758  |
| REM     | 0.744 | 0.745 | 0.7338 | 0.7349 |
| DLA     | 0.754 | 0.750 | 0.7527 | 0.5144 |
| PairD   | 0.737 | 0.749 | -      | -      |
| DBGD    | -     | 0.672 | -      | -      |
| PDGD    | 0.533 | 0.756 | -      | -      |
| NA      | 0.738 | 0.740 | 0.7274 | 0.763  |
| DP_DCG  | -     | -     | 0.7383 | 0.7575 |

### run 2

- 5k iteration
- valid, test: rerun run 1 setup
- test_prev: results from run 1 (from previous table)
- valid_simu, test_simu: validation values are against simulated click

| ndcg@10 | valid    | valid_simu | test_prev | test     | test_simu | original |
| ------- | -------- | ---------- | --------- | -------- | --------- | -------- |
| IPW     | 0.7498/2 | 0.2659/4   | 0.7570/1  | 0.7563/1 | 0.7583/1  | 0.755/1  |
| REM     | 0.7259/3 | 0.2992/2   | 0.7338/3  | 0.7319/3 | 0.7372/3  | 0.744/3  |
| DLA     | 0.7500/1 | 0.2905/3   | 0.7527/2  | 0.7562/2 | 0.7566/2  | 0.754/2  |
| PDGD    | 0.4716/5 | 0.0863/5   | -         | 0.4719/5 | 0.4721/5  | 0.533/5  |
| NA      | 0.7240/4 | 0.3109/1   | 0.7274/4  | 0.7305/4 | 0.7289/4  | 0.738/4  |
| DP_DCG  |          | 0.2599     | 0.7383    |          | 0.7304    |          |

### conclusion

- anova 1 way of (IPW, REM, DLA, NA), with N=3 (still too small!) p-value is 0.0001
- paired t test of (IPW, DLA)(top 2) gives unsignificant result
- validate results with simulated click gives uncreditable rankings
- validate results with real label gives more creditable rankings (duh,)

## Deep fm

### run 1

- 5k iteration
- deepfm without layer norm

--------- |    without      |      with       |
| ndcg@10 | val    | test   | val    | test   |
| ------- | ------ | ------ | ------ | ------ |
| ipw     | 0.7385 | 0.7586 | 0.6457 | 0.7158 |
| dla     | 0.7282 | 0.7563 | 0.5335 | 0.5168 |
| na      | 0.7276 | 0.727  | 0.5372 | 0.692  |
| dp_dcg  | 0.7182 | 0.7319 | 0.5714 | 0.578  |

### run 2

- 10k iteration
- deepfm with layer norm

--------- |    without      |      with       |
| ndcg@10 | val    | test   | val    | test   |
| ------- | ------ | ------ | ------ | ------ |
| ipw     | 0.755  | 0.7624 | 0.7447 | 0.7493 |
| dla     | 0.7539 | 0.7627 | 0.7422 | 0.7481 |
| na      | 0.7186 | 0.7261 | 0.7146 | 0.7179 |
| dp_dcg  | 0.7304 | 0.7365 | 0.6138 | 0.6126 |

## Deepfm with categorical on collection

dataset:

| train_query | train_user | train_serie |
| ----------- | ---------- | ----------- |
| 185728      | 88494      | 5585        |

| eval_query | eval_user | eval_serie | eval_seen_user | eval_seen_serie |
| ---------- | --------- | ---------- | -------------- | --------------- |
| 103686     | 58517     | 5364       | 42241 (0.72)   | 5255 (0.98)     |

| test_query | test_user | test_serie | test_seen_user | test_seen_serie |
| ---------- | --------- | ---------- | -------------- | --------------- |
| 110804     | 63349     | 5311       | 34697 (0.55)   | 5173 (0.97)     |

1041151,375356,486055,_find_pattern_same_len

| features  | architecture | init | valid  | test   |
| --------- | ------------ | ---- | ------ | ------ |
| full      | deepfm       | y    | 0.7483 | 0.7391 |
| full      | deepfm       |      | 0.744  | 0.7299 |
| no_cate   | deepfm       |      | 0.7462 | 0.7424 |
| no_cate   | dnn          |      | 0.7412 | 0.7388 |
| cate_only | deepfm       | y    | 0.6603 | 0.6245 |
| cate_only | deepfm       |      | 0.6439 | 0.6213 |
| cate_only | dnn          |      | 0.5916 | 0.602  |


