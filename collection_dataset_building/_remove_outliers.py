import numpy as np
import pandas as pd
from scipy.special import inv_boxcox
from scipy.stats import boxcox, percentileofscore


def _inv_bc(x_b, b, x_bs, xs):
    x = inv_boxcox(x_b, b)
    if np.isnan(x):
        x = np.percentile(xs, percentileofscore(x_bs, x_b))

    return x


def _get_bound(serie):
    bx, b = boxcox(serie)

    boxcox_25 = np.quantile(bx, 0.25)
    boxcox_75 = np.quantile(bx, 0.75)
    boxcox_IQR = boxcox_75 - boxcox_25

    boxcoxed_bound_down = boxcox_25 - 1.5*boxcox_IQR
    boxcoxed_bound_up = boxcox_75 + 1.5*boxcox_IQR
    inv_bound_up = _inv_bc(boxcoxed_bound_up, b, bx, serie)
    inv_bound_down = _inv_bc(boxcoxed_bound_down, b, bx, serie)

    return inv_bound_down, inv_bound_up

# USER_NO_CLICK_DOWN, USER_NO_CLICK_UP
# USER_NO_SESSION_DOWN, USER_NO_SESSION_UP
# USER_NO_CLICK_BY_SESSION_DOWN, USER_NO_CLICK_BY_SESSION_UP
# USER_NO_CLICK_BY_SESSION_COLLECTION_DOWN, USER_NO_CLICK_BY_SESSION_COLLECTION_UP

# SERIE_NO_COLLECTION_DOWN, SERIE_NO_COLLECTION_UP
# SERIE_NO_CLICK_DOWN, SERIE_NO_CLICK_UP
# SERIE_NO_SEEN_DOWN, SERIE_NO_SEEN_UP

# COLLECTION_NO_SESSION_DOWN, COLLECTION_NO_SESSION_UP


def _remove_outlier(df, mask, on, mode='keep', verbose=False):
    _before = len(df)

    if mode == 'keep':
        df = df[df[on].isin(mask)]
    elif mode == 'mask':
        df = df[~df[on].isin(mask)]
    else:
        raise Exception(f'unknown mode {mode}')

    _after = len(df)

    if verbose:
        print(
            f'(before, after, %_eliminated) = ({_before}, {_after}, {1. - _after/_before})')
    return df


def remove_outlier(df, verbose=False):

    # getting bounds
    user_no_click_serie = df[df.label > 0].groupby('user_id').size()
    USER_NO_CLICK_DOWN, USER_NO_CLICK_UP = _get_bound(user_no_click_serie)

    user_no_session_serie = df.groupby(['user_id', ]).session_start.nunique()
    USER_NO_SESSION_DOWN, USER_NO_SESSION_UP = _get_bound(user_no_session_serie)

    user_no_click_by_session_serie = df[df.label > 0].groupby(
        ['user_id', 'session_start']).size()
    USER_NO_CLICK_BY_SESSION_DOWN, USER_NO_CLICK_BY_SESSION_UP = _get_bound(
        user_no_click_by_session_serie)

    user_no_click_by_session_collection_serie = df[df.label > 0].groupby(
        ['user_id', 'session_start', 'collection']).size()
    USER_NO_CLICK_BY_SESSION_COLLECTION_DOWN, USER_NO_CLICK_BY_SESSION_COLLECTION_UP = _get_bound(
        user_no_click_by_session_collection_serie)

    serie_no_collection_serie = df.groupby(['serie']).collection.nunique()
    SERIE_NO_COLLECTION_DOWN, SERIE_NO_COLLECTION_UP = _get_bound(serie_no_collection_serie)

    serie_no_click_serie = df.groupby(['serie']).label.apply(lambda x: pd.Series(x > 0).sum()) + 1
    SERIE_NO_CLICK_DOWN, SERIE_NO_CLICK_UP = _get_bound(serie_no_click_serie)

    serie_no_seen_serie = df.groupby(['serie']).size()
    SERIE_NO_SEEN_DOWN, SERIE_NO_SEEN_UP = _get_bound(serie_no_seen_serie)

    collection_no_session_serie = df.groupby(['collection', 'session_start', 'user_id']).size(
    ).reset_index().groupby('collection').size()
    COLLECTION_NO_SESSION_DOWN, COLLECTION_NO_SESSION_UP = _get_bound(collection_no_session_serie)

    old_e, old_u, old_s, old_c = len(df), df.user_id.nunique(
    ), df.serie.nunique(), df.collection.nunique()

    if verbose:
        print(
            f'Before: (#_example, #_user, #_serie, #_collection) = {len(df),df.user_id.nunique(),df.serie.nunique(),df.collection.nunique()}'
        )

    # user_no_click
    if verbose:
        print(
            f'# clicks by user: bounds:{USER_NO_CLICK_DOWN:.2f}, {USER_NO_CLICK_UP:.2f}')
    serie = user_no_click_serie
    keeps = serie[(serie <= USER_NO_CLICK_UP) & (
        serie >= USER_NO_CLICK_DOWN)].index.values
    df = _remove_outlier(df, keeps, 'user_id', verbose=verbose)

    # user_no_session
    if verbose:
        print(
            f'# session by user: bounds:{USER_NO_SESSION_DOWN:.2f}, {USER_NO_SESSION_UP:.2f}')
    serie = user_no_session_serie
    keeps = serie[(serie <= USER_NO_SESSION_UP) & (
        serie >= USER_NO_SESSION_DOWN)].index.values
    df = _remove_outlier(df, keeps, 'user_id', verbose=verbose)

    # user_no_click_by_session
    if verbose:
        print(
            f'# clicks by session: bounds:{USER_NO_CLICK_BY_SESSION_DOWN:.2f}, {USER_NO_CLICK_BY_SESSION_UP:.2f}')
    serie = user_no_click_by_session_serie
    keeps = serie[(serie <= USER_NO_CLICK_BY_SESSION_UP) & (
        serie >= USER_NO_CLICK_BY_SESSION_DOWN)].index.droplevel(1).unique()
    df = _remove_outlier(df, keeps, 'user_id', verbose=verbose)

    # USER_NO_CLICK_BY_SESSION_COLLECTION
    if verbose:
        print(
            f'# clicks by session, collection: bounds:{USER_NO_CLICK_BY_SESSION_COLLECTION_DOWN:.2f}, {USER_NO_CLICK_BY_SESSION_COLLECTION_UP:.2f}')
    serie = user_no_click_by_session_collection_serie
    keeps = serie[(serie <= USER_NO_CLICK_BY_SESSION_COLLECTION_UP) & (
        serie >= USER_NO_CLICK_BY_SESSION_COLLECTION_DOWN)].index.droplevel(1).droplevel(1).unique()
    df = _remove_outlier(df, keeps, 'user_id', verbose=verbose)

    # SERIE_NO_COLLECTION
    if verbose:
        print(
            f'# collection belonged to: bounds:{SERIE_NO_COLLECTION_DOWN:.2f}, {SERIE_NO_COLLECTION_UP:.2f}')
    serie = serie_no_collection_serie
    keeps = serie[(serie <= SERIE_NO_COLLECTION_UP) & (
        serie >= SERIE_NO_COLLECTION_DOWN)].index.unique()
    df = _remove_outlier(df, keeps, 'serie', verbose=verbose)

    # SERIE_NO_CLICK
    if verbose:
        print(
            f'# clicks of a serie: bounds:{SERIE_NO_CLICK_DOWN:.2f}, {SERIE_NO_CLICK_UP:.2f}')
    serie = serie_no_click_serie
    keeps = serie[(serie <= SERIE_NO_CLICK_UP) & (
        serie >= SERIE_NO_CLICK_DOWN)].index.unique()
    # keeps
    df = _remove_outlier(df, keeps, 'serie', verbose=verbose)

    # SERIE_NO_SEEN
    if verbose:
        print(
            f'# seens of a serie: bounds:{SERIE_NO_SEEN_DOWN:.2f}, {SERIE_NO_SEEN_UP:.2f}')
    serie = serie_no_seen_serie
    keeps = serie[(serie <= SERIE_NO_SEEN_UP) & (
        serie >= SERIE_NO_SEEN_DOWN)].index.unique()
    # keeps
    df = _remove_outlier(df, keeps, 'serie', verbose=verbose)

    # COLLECTION_NO_SESSION
    if verbose:
        print(
            f'# session contain a collection: bounds:{COLLECTION_NO_SESSION_DOWN:.2f}, {COLLECTION_NO_SESSION_UP:.2f}')
    serie = df.groupby(['collection', 'session_start', 'user_id']).size(
    ).reset_index().groupby('collection').size()
    keeps = serie[(serie <= COLLECTION_NO_SESSION_UP) & (
        serie >= COLLECTION_NO_SESSION_DOWN)].index.unique()
    # keeps
    df = _remove_outlier(df, keeps, 'collection', verbose=verbose)

    new_e, new_u, new_s, new_c = len(df), df.user_id.nunique(
    ), df.serie.nunique(), df.collection.nunique()
    if verbose:
        print(
            f'After: (#_example, #_user, #_serie, #_collection) = {new_e,new_u,new_s,new_c}')
        print(f'Coverage (#_example, #_user, #_serie, #_collection):')
        print(','.join([f'{f:.4}' for f in [new_e/old_e,
                                            new_u/old_u, new_s/old_s, new_c/old_c]]))

    return df
