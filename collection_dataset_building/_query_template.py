AUDIENCE_GENRES_LATENT = r'''
--sql
with a as (select 'a')

, audience_genres as (
select
  cast(split(oid,'-')[offset(2)] as int64) as audience,
--   query_string,
  REGEXP_EXTRACT_ALL(query_string,'(?:genreScore|chaptersReadInGenre|seriesReadInGenre)\\((\\d+)\\)') as audience_genre
from `ml_sample.audience_query`
)

, audience_genres_genre as (
select
 distinct cast(audience_genre as int64) as audience_genre,
 audience,
from
  audience_genres,
  unnest(audience_genre) as audience_genre
)

, audience_genres_genre_latent as (
select
  audience,
  gl.*
from
  audience_genres_genre inner join `ml_sample.genre_latent` gl on
    audience_genres_genre.audience_genre = gl.genre
)

, audience_genres_latent as (
  select
    audience,
    avg(latent_0) as latent_0, avg(latent_1) as latent_1, avg(latent_2) as latent_2, avg(latent_3) as latent_3, avg(latent_4) as latent_4, avg(latent_5) as latent_5, avg(latent_6) as latent_6, avg(latent_7) as latent_7, avg(latent_8) as latent_8, avg(latent_9) as latent_9, avg(latent_10) as latent_10, avg(latent_11) as latent_11, avg(latent_12) as latent_12, avg(latent_13) as latent_13, avg(latent_14) as latent_14, avg(latent_15) as latent_15, avg(latent_16) as latent_16, avg(latent_17) as latent_17, avg(latent_18) as latent_18, avg(latent_19) as latent_19, avg(latent_20) as latent_20, avg(latent_21) as latent_21, avg(latent_22) as latent_22, avg(latent_23) as latent_23, avg(latent_24) as latent_24, avg(latent_25) as latent_25, avg(latent_26) as latent_26, avg(latent_27) as latent_27, avg(latent_28) as latent_28, avg(latent_29) as latent_29, avg(latent_30) as latent_30, avg(latent_31) as latent_31, avg(latent_32) as latent_32, avg(latent_33) as latent_33, avg(latent_34) as latent_34, avg(latent_35) as latent_35, avg(latent_36) as latent_36, avg(latent_37) as latent_37, avg(latent_38) as latent_38, avg(latent_39) as latent_39, avg(latent_40) as latent_40, avg(latent_41) as latent_41, avg(latent_42) as latent_42, avg(latent_43) as latent_43, avg(latent_44) as latent_44, avg(latent_45) as latent_45, avg(latent_46) as latent_46, avg(latent_47) as latent_47, avg(latent_48) as latent_48, avg(latent_49) as latent_49
  from
    audience_genres_genre_latent
  group by
    audience
)
-------------------
select * from audience_genres_latent
;
'''

AUDIENCE_USERS_LATENT = '''
--sql
with a as (select 'a')

, audience_users_user_latent as (
select
  cast(split(audience_oid, '-')[offset(2)] as int64) as audience,
  ul.*
from
  `ml_sample.audience_user` au inner join `ml_sample.user_latent` ul on
    au.uid  = ul.uid
)

, audience_users_latent as (
  select
    audience,
    avg(latent_0) as latent_0, avg(latent_1) as latent_1, avg(latent_2) as latent_2, avg(latent_3) as latent_3, avg(latent_4) as latent_4, avg(latent_5) as latent_5, avg(latent_6) as latent_6, avg(latent_7) as latent_7, avg(latent_8) as latent_8, avg(latent_9) as latent_9, avg(latent_10) as latent_10, avg(latent_11) as latent_11, avg(latent_12) as latent_12, avg(latent_13) as latent_13, avg(latent_14) as latent_14, avg(latent_15) as latent_15, avg(latent_16) as latent_16, avg(latent_17) as latent_17, avg(latent_18) as latent_18, avg(latent_19) as latent_19, avg(latent_20) as latent_20, avg(latent_21) as latent_21, avg(latent_22) as latent_22, avg(latent_23) as latent_23, avg(latent_24) as latent_24, avg(latent_25) as latent_25, avg(latent_26) as latent_26, avg(latent_27) as latent_27, avg(latent_28) as latent_28, avg(latent_29) as latent_29, avg(latent_30) as latent_30, avg(latent_31) as latent_31, avg(latent_32) as latent_32, avg(latent_33) as latent_33, avg(latent_34) as latent_34, avg(latent_35) as latent_35, avg(latent_36) as latent_36, avg(latent_37) as latent_37, avg(latent_38) as latent_38, avg(latent_39) as latent_39, avg(latent_40) as latent_40, avg(latent_41) as latent_41, avg(latent_42) as latent_42, avg(latent_43) as latent_43, avg(latent_44) as latent_44, avg(latent_45) as latent_45, avg(latent_46) as latent_46, avg(latent_47) as latent_47, avg(latent_48) as latent_48, avg(latent_49) as latent_49
  from
    audience_users_user_latent
  group by
    audience
)
-------------------
select * from audience_users_latent
;
'''

COLLECTION_GENRES_LATENT = '''
--sql
with a as (select 1)

, collection_genres as (
  select
    oid as collection,
    cast(key_genre as int64) as genre
  from
    `ml_sample.collection` c
    , unnest(split(c.key_genre,',')) as key_genre
)

, collection_genres_genre_latent as (
  select
    collection,
    gl.*
  from
    collection_genres inner join `ml_sample.genre_latent` gl on
      collection_genres.genre = gl.genre
)

, collection_genres_latent as (
  select
    collection,
    avg(latent_0) as latent_0, avg(latent_1) as latent_1, avg(latent_2) as latent_2, avg(latent_3) as latent_3, avg(latent_4) as latent_4, avg(latent_5) as latent_5, avg(latent_6) as latent_6, avg(latent_7) as latent_7, avg(latent_8) as latent_8, avg(latent_9) as latent_9, avg(latent_10) as latent_10, avg(latent_11) as latent_11, avg(latent_12) as latent_12, avg(latent_13) as latent_13, avg(latent_14) as latent_14, avg(latent_15) as latent_15, avg(latent_16) as latent_16, avg(latent_17) as latent_17, avg(latent_18) as latent_18, avg(latent_19) as latent_19, avg(latent_20) as latent_20, avg(latent_21) as latent_21, avg(latent_22) as latent_22, avg(latent_23) as latent_23, avg(latent_24) as latent_24, avg(latent_25) as latent_25, avg(latent_26) as latent_26, avg(latent_27) as latent_27, avg(latent_28) as latent_28, avg(latent_29) as latent_29, avg(latent_30) as latent_30, avg(latent_31) as latent_31, avg(latent_32) as latent_32, avg(latent_33) as latent_33, avg(latent_34) as latent_34, avg(latent_35) as latent_35, avg(latent_36) as latent_36, avg(latent_37) as latent_37, avg(latent_38) as latent_38, avg(latent_39) as latent_39, avg(latent_40) as latent_40, avg(latent_41) as latent_41, avg(latent_42) as latent_42, avg(latent_43) as latent_43, avg(latent_44) as latent_44, avg(latent_45) as latent_45, avg(latent_46) as latent_46, avg(latent_47) as latent_47, avg(latent_48) as latent_48, avg(latent_49) as latent_49
  from
    collection_genres_genre_latent
  group by
    collection
)
select * from collection_genres_latent
;
'''

COLLECTION_SERIES_LATENT = '''
--sql
with a as (select 1)

, collection_series_serie_latent as(
  select
    *
  except (layout_data,serie_order,mid)
  from
    `ml_sample.collection_serie` cs inner join `ml_sample.serie_latent` as sl on
      cs.serie  = cast(sl.mid as int64)
)

, collection_latent as (
  select
    oid as collection,
    avg(latent_0) as latent_0, avg(latent_1) as latent_1, avg(latent_2) as latent_2, avg(latent_3) as latent_3, avg(latent_4) as latent_4, avg(latent_5) as latent_5, avg(latent_6) as latent_6, avg(latent_7) as latent_7, avg(latent_8) as latent_8, avg(latent_9) as latent_9, avg(latent_10) as latent_10, avg(latent_11) as latent_11, avg(latent_12) as latent_12, avg(latent_13) as latent_13, avg(latent_14) as latent_14, avg(latent_15) as latent_15, avg(latent_16) as latent_16, avg(latent_17) as latent_17, avg(latent_18) as latent_18, avg(latent_19) as latent_19, avg(latent_20) as latent_20, avg(latent_21) as latent_21, avg(latent_22) as latent_22, avg(latent_23) as latent_23, avg(latent_24) as latent_24, avg(latent_25) as latent_25, avg(latent_26) as latent_26, avg(latent_27) as latent_27, avg(latent_28) as latent_28, avg(latent_29) as latent_29, avg(latent_30) as latent_30, avg(latent_31) as latent_31, avg(latent_32) as latent_32, avg(latent_33) as latent_33, avg(latent_34) as latent_34, avg(latent_35) as latent_35, avg(latent_36) as latent_36, avg(latent_37) as latent_37, avg(latent_38) as latent_38, avg(latent_39) as latent_39, avg(latent_40) as latent_40, avg(latent_41) as latent_41, avg(latent_42) as latent_42, avg(latent_43) as latent_43, avg(latent_44) as latent_44, avg(latent_45) as latent_45, avg(latent_46) as latent_46, avg(latent_47) as latent_47, avg(latent_48) as latent_48, avg(latent_49) as latent_49
  from
    collection_series_serie_latent
  group by
    oid
)

select * from collection_latent
;
'''


# params = {
#     'session_collection_clicks': 'ml_sample.session_collection_clicks_20191201_20191218',
#     'user_history_long': 'ml_sample.user_history_long_20191201',
#     'user_history_short': 'ml_sample.user_history_short_20191201_20191218',
#     'user_serie_history': 'ml_sample.user_serie_history_20191201_20191218',
# }
# DATA_ENSEMBLE.format(**params)
DATA_ENSEMBLE = f'''
--sql
with a as (select 1)

, collection_serie as (
  select
    oid as collection,
    serie,
    ROW_NUMBER() over (partition by oid order by serie_order) as serie_order
  from `ml_sample.collection_serie` 
)

, user_session_clicked_serie_with_order_in_collection as (
  select distinct
    user_id,
    session_start,
    cc.collection, 
    clicked_serie,
    serie_order,
    greatest(
      if(read_count>0 or open_count>0, 1, 0),
      if(read_count>=2, 2, 0),
      if(read_count>=5 or bookmark_count>0 or download_count>0, 3, 0)
    ) as label
  from
    `{{session_collection_clicks}}` cc inner join collection_serie cs on
      cc.clicked_serie = cs.serie and
      cc.collection = cs.collection   
)

, user_session_collection_max_clicked_order as (
  select
    user_id,
    session_start,
    collection,
    max(serie_order) as max_clicked_order
  from
    user_session_clicked_serie_with_order_in_collection
  group by
    user_id,
    session_start,
    collection  
)

, user_session_seen_serie as (
  select
    uscmco.user_id,
    uscmco.session_start,
    uscmco.collection,
    serie,
    ifnull(label, 0) as label,
    collection_serie.serie_order
  from user_session_collection_max_clicked_order uscmco
    inner join collection_serie
      using(collection)
    left join user_session_clicked_serie_with_order_in_collection uscswouc 
      on
        uscmco.user_id = uscswouc.user_id and
        uscmco.session_start = uscswouc.session_start and
        uscmco.collection = uscswouc.collection and
        collection_serie.serie = uscswouc.clicked_serie      
  where
    collection_serie.serie_order <= ceil(uscmco.max_clicked_order/3)*3
)

, features_agg as (
  select
    user_session_seen_serie.user_id as user_id,
    user_session_seen_serie.session_start as session_start,
    user_session_seen_serie.collection as collection,
    user_session_seen_serie.serie as serie,
    user_session_seen_serie.serie_order as serie_order,
    user_session_seen_serie.label as label,
    user_categorical_id.id as user_categorical_id,
    serie_categorical_id.id as serie_categorical_id,
    
    --- user_serie_history,
    user_serie_history.prev_time as prev_time,
    
    --- user_latent
    {', '.join(f'user_latent.latent_{i} as user_latent_{i}' for i in range(50))},
    
    --- user_history_short,
    {', '.join(f'user_history_short.latent_{i} as user_history_short_latent_{i}' for i in range(50))},
    
    --- user_history_long,
    {', '.join(f'user_history_long.latent_{i} as user_history_long_latent_{i}' for i in range(50))},
    
    --- user_audiences_users_latent,
    {', '.join(f'user_audiences_users_latent.latent_{i} as user_audiences_users_latent_{i}' for i in range(50))},
    
    --- user_audiences_genres_latent,
    {', '.join(f'user_audiences_genres_latent.latent_{i} as user_audiences_genres_latent_{i}' for i in range(50))},
    
    --- collection_genres_latent,
    {', '.join(f'collection_genres_latent.latent_{i} as collection_genres_latent_{i}' for i in range(50))},
    
    --- collection_series_latent,
    {', '.join(f'collection_series_latent.latent_{i} as collection_series_latent_{i}' for i in range(50))},
    
    --- serie_latent,
    {', '.join(f'serie_latent.latent_{i} as serie_latent_{i}' for i in range(50))}
    
  from
    user_session_seen_serie
      left join `ml_sample.collection_genres_latent` collection_genres_latent  
        using (collection)
      left join `{{user_history_long}}` user_history_long 
        using (user_id)
      left join `ml_sample.collection_series_latent` collection_series_latent  
        using (collection)
      left join `ml_sample.serie_latent` serie_latent  
        on serie_latent.mid = user_session_seen_serie.serie
      left join `{{user_history_short}}` user_history_short
        using (user_id, session_start)
      left join `ml_sample.user_latent` user_latent
        on user_session_seen_serie.user_id = user_latent.uid
      left join `ml_sample.user_audiences_users_latent` user_audiences_users_latent  
        on user_session_seen_serie.user_id = user_audiences_users_latent.uid
      left join `ml_sample.user_audiences_genres_latent` user_audiences_genres_latent  
        on user_session_seen_serie.user_id = user_audiences_genres_latent.uid
      left join `{{user_serie_history}}` user_serie_history 
        on user_session_seen_serie.user_id = user_serie_history.user_id and
          user_session_seen_serie.session_start = user_serie_history.session_start and
          user_session_seen_serie.serie = user_serie_history.clicked_serie
      inner join `ml_sample.user_uid_to_id` user_categorical_id
        on user_categorical_id.uid = user_session_seen_serie.user_id
      inner join `ml_sample.serie_mid_to_id` serie_categorical_id
        on serie_categorical_id.mid = serie
) 

, qid as (
 select distinct
  user_id, session_start, collection, row_number() over() as qid
 from 
  (select distinct user_id, session_start, collection from features_agg)
)

, features_agg_qid as(
  select
    qid.qid as qid,

    user_id,
    session_start,
    collection,
    serie,
    serie_order,
    label,
    user_categorical_id,
    serie_categorical_id,
    prev_time,    
    {', '.join(f'user_latent_{i}' for i in range(50))},    
    {', '.join(f'user_history_short_latent_{i}' for i in range(50))},    
    {', '.join(f'user_history_long_latent_{i}' for i in range(50))},    
    {', '.join(f'user_audiences_users_latent_{i}' for i in range(50))},    
    {', '.join(f'user_audiences_genres_latent_{i}' for i in range(50))},    
    {', '.join(f'collection_genres_latent_{i}' for i in range(50))},    
    {', '.join(f'collection_series_latent_{i}' for i in range(50))},    
    {', '.join(f'serie_latent_{i}' for i in range(50))}

  from features_agg inner join qid using (user_id, session_start, collection)
)

select * from features_agg_qid 
--order by user_id, session_start, collection, serie_order
; 
'''

# params = {
#     'session_collection_clicks': 'ml_sample.session_collection_clicks_20191201_20191218',
#     'user_history_long': 'ml_sample.user_history_long_20191201',
#     'user_history_short': 'ml_sample.user_history_short_20191201_20191218',
#     'user_serie_history': 'ml_sample.user_serie_history_20191201_20191218',
# }
# DATA_ENSEMBLE_SINGLE_CLICK.format(**params)
# like DATA_ENSEMBLE but applied at the end with
# df = df[df.label > 0][['user_id', 'session_start','collection', 'serie', 'label']]\
# .merge(df,on=['user_id', 'session_start','collection'], suffixes=('_c', ''))
# len(df)
DATA_ENSEMBLE_SINGLE_CLICK = f'''
--sql
with a as (select 1)

, collection_serie as (
  select
    oid as collection,
    serie,
    ROW_NUMBER() over (partition by oid order by serie_order) as serie_order
  from `ml_sample.collection_serie` 
)

, user_session_clicked_serie_with_order_in_collection as (
  select distinct
    user_id,
    session_start,
    cc.collection, 
    clicked_serie,
    serie_order,
    greatest(
      if(read_count>0 or open_count>0, 1, 0),
      if(read_count>=2, 2, 0),
      if(read_count>=5 or bookmark_count>0 or download_count>0, 3, 0)
    ) as label
  from
    `{{session_collection_clicks}}` cc inner join collection_serie cs on
      cc.clicked_serie = cs.serie and
      cc.collection = cs.collection   
)

, user_session_collection_max_clicked_order as (
  select
    user_id,
    session_start,
    collection,
    max(serie_order) as max_clicked_order
  from
    user_session_clicked_serie_with_order_in_collection
  group by
    user_id,
    session_start,
    collection  
)

, user_session_seen_serie as (
  select
    uscmco.user_id,
    uscmco.session_start,
    uscmco.collection,
    serie,
    ifnull(label, 0) as label,
    collection_serie.serie_order
  from user_session_collection_max_clicked_order uscmco
    inner join collection_serie
      using(collection)
    left join user_session_clicked_serie_with_order_in_collection uscswouc 
      on
        uscmco.user_id = uscswouc.user_id and
        uscmco.session_start = uscswouc.session_start and
        uscmco.collection = uscswouc.collection and
        collection_serie.serie = uscswouc.clicked_serie      
  where
    collection_serie.serie_order <= ceil(uscmco.max_clicked_order/3)*3
)

, features_agg as (
  select
    user_session_seen_serie.user_id as user_id,
    user_session_seen_serie.session_start as session_start,
    user_session_seen_serie.collection as collection,
    user_session_seen_serie.serie as serie,
    user_session_seen_serie.serie_order as serie_order,
    user_session_seen_serie.label as label,
    
    --- user_serie_history,
    user_serie_history.prev_time as prev_time,
    
    --- user_latent
    {', '.join(f'user_latent.latent_{i} as user_latent_{i}' for i in range(50))},
    
    --- user_history_short,
    {', '.join(f'user_history_short.latent_{i} as user_history_short_latent_{i}' for i in range(50))},
    
    --- user_history_long,
    {', '.join(f'user_history_long.latent_{i} as user_history_long_latent_{i}' for i in range(50))},
    
    --- user_audiences_users_latent,
    {', '.join(f'user_audiences_users_latent.latent_{i} as user_audiences_users_latent_{i}' for i in range(50))},
    
    --- user_audiences_genres_latent,
    {', '.join(f'user_audiences_genres_latent.latent_{i} as user_audiences_genres_latent_{i}' for i in range(50))},
    
    --- collection_genres_latent,
    {', '.join(f'collection_genres_latent.latent_{i} as collection_genres_latent_{i}' for i in range(50))},
    
    --- collection_series_latent,
    {', '.join(f'collection_series_latent.latent_{i} as collection_series_latent_{i}' for i in range(50))},
    
    --- serie_latent,
    {', '.join(f'serie_latent.latent_{i} as serie_latent_{i}' for i in range(50))}
    
  from
    user_session_seen_serie
      left join `ml_sample.collection_genres_latent` collection_genres_latent  
        using (collection)
      left join `{{user_history_long}}` user_history_long 
        using (user_id)
      left join `ml_sample.collection_series_latent` collection_series_latent  
        using (collection)
      left join `ml_sample.serie_latent` serie_latent  
        on serie_latent.mid = user_session_seen_serie.serie
      left join `{{user_history_short}}` user_history_short
        using (user_id, session_start)
      left join `ml_sample.user_latent` user_latent
        on user_session_seen_serie.user_id = user_latent.uid
      left join `ml_sample.user_audiences_users_latent` user_audiences_users_latent  
        on user_session_seen_serie.user_id = user_audiences_users_latent.uid
      left join `ml_sample.user_audiences_genres_latent` user_audiences_genres_latent  
        on user_session_seen_serie.user_id = user_audiences_genres_latent.uid
      left join `{{user_serie_history}}` user_serie_history 
        on user_session_seen_serie.user_id = user_serie_history.user_id and
          user_session_seen_serie.session_start = user_serie_history.session_start and
          user_session_seen_serie.serie = user_serie_history.clicked_serie
)

, features_agg_single_click as (
  select
    user_id,
    session_start,
    collection,
    serie,
    serie_order,
    if(target_serie = serie, label, 0) as label,
    target_serie,
    prev_time,
    {', '.join(f'user_latent_{i}' for i in range(50))},
    {', '.join(f'user_history_short_latent_{i}' for i in range(50))},
    {', '.join(f'user_history_long_latent_{i}' for i in range(50))},
    {', '.join(f'user_audiences_users_latent_{i}' for i in range(50))},
    {', '.join(f'user_audiences_genres_latent_{i}' for i in range(50))},
    {', '.join(f'collection_genres_latent_{i}' for i in range(50))},
    {', '.join(f'collection_series_latent_{i}' for i in range(50))},
    {', '.join(f'serie_latent_{i}' for i in range(50))}

  from features_agg inner join
    (select user_id, session_start, collection, serie as target_serie from features_agg where label > 0) target 
      using (user_id, session_start, collection)
)

, qid as (
 select distinct
  user_id, session_start, collection, target_serie, row_number() over() as qid
 from 
  (select distinct user_id, session_start, collection, target_serie from features_agg_single_click)
)

, features_agg_single_click_qid as(
  select
    qid.qid as qid,
    target_serie,

    user_id,
    session_start,
    collection,
    serie,
    serie_order,
    label,
    prev_time,    
    {', '.join(f'user_latent_{i}' for i in range(50))},    
    {', '.join(f'user_history_short_latent_{i}' for i in range(50))},    
    {', '.join(f'user_history_long_latent_{i}' for i in range(50))},    
    {', '.join(f'user_audiences_users_latent_{i}' for i in range(50))},    
    {', '.join(f'user_audiences_genres_latent_{i}' for i in range(50))},    
    {', '.join(f'collection_genres_latent_{i}' for i in range(50))},    
    {', '.join(f'collection_series_latent_{i}' for i in range(50))},    
    {', '.join(f'serie_latent_{i}' for i in range(50))}
    
  from features_agg_single_click inner join qid using (user_id, session_start, collection, target_serie)
)

select * from features_agg_single_click_qid
--order by user_id, session_start, collection, serie_order
;
'''

GENRE_LATENT = '''
--sql
with a as (select 1)

, genre_series_serie_latent as(
  select
    *
  except (key, mid)
  from
    `ml_sample.manga_genre` mg inner join `ml_sample.serie_latent` as sl on
      mg.oid  = cast(sl.mid as int64) and
      key = 1
)

, genre_latent as (
  select
    genre_oid as genre,
    name as genre_name,
    avg(latent_0) as latent_0, avg(latent_1) as latent_1, avg(latent_2) as latent_2, avg(latent_3) as latent_3, avg(latent_4) as latent_4, avg(latent_5) as latent_5, avg(latent_6) as latent_6, avg(latent_7) as latent_7, avg(latent_8) as latent_8, avg(latent_9) as latent_9, avg(latent_10) as latent_10, avg(latent_11) as latent_11, avg(latent_12) as latent_12, avg(latent_13) as latent_13, avg(latent_14) as latent_14, avg(latent_15) as latent_15, avg(latent_16) as latent_16, avg(latent_17) as latent_17, avg(latent_18) as latent_18, avg(latent_19) as latent_19, avg(latent_20) as latent_20, avg(latent_21) as latent_21, avg(latent_22) as latent_22, avg(latent_23) as latent_23, avg(latent_24) as latent_24, avg(latent_25) as latent_25, avg(latent_26) as latent_26, avg(latent_27) as latent_27, avg(latent_28) as latent_28, avg(latent_29) as latent_29, avg(latent_30) as latent_30, avg(latent_31) as latent_31, avg(latent_32) as latent_32, avg(latent_33) as latent_33, avg(latent_34) as latent_34, avg(latent_35) as latent_35, avg(latent_36) as latent_36, avg(latent_37) as latent_37, avg(latent_38) as latent_38, avg(latent_39) as latent_39, avg(latent_40) as latent_40, avg(latent_41) as latent_41, avg(latent_42) as latent_42, avg(latent_43) as latent_43, avg(latent_44) as latent_44, avg(latent_45) as latent_45, avg(latent_46) as latent_46, avg(latent_47) as latent_47, avg(latent_48) as latent_48, avg(latent_49) as latent_49
  from
    genre_series_serie_latent
  group by
    genre_oid, name
)

select * from genre_latent
;
'''

# params = {
#     'date_begin': datetime(2019, 12, 9),
#     'date_end': datetime(2019, 12, 18),
#     'session_time_out': 60
# }
# SESSION_COLLECTION_CLICKS.format(**params)
SESSION_COLLECTION_CLICKS = '''
--sql
with a as (select 'a')
--- base -------------------------------------
--- raw events
-- name: events
-- col event_timestamp: timestamp
-- col event_name: str
-- col user_id: str
-- col event_params: k:v
, raw_events as (
  select
    event_timestamp,
    event_name,
    user_id,
    event_params
  from
    `ml_sample.android_event_new_*`
    WHERE (_TABLE_SUFFIX between '{date_begin:%Y%m%d}' and '{date_end:%Y%m%d}')
    and user_id like "_%" # clean na
    and date(event_timestamp) between '{date_begin:%Y-%m-%d}' and '{date_end:%Y-%m-%d}'
)
, raw_events_w_first_session_flag as (
-- ts, event_name, uid, param, is_1st_session_event
  select
    *
    except (prev_event),
    if(
--       timestamp_diff(event_timestamp, prev_event, MINUTE) > @SESSION_TIMEOUT or prev_event is null,
      timestamp_diff(event_timestamp, prev_event,
                     MINUTE) > {session_time_out} or prev_event is null,
      1,
      0
    ) as is_first_session_event
  from
    (
      select *, lag(event_timestamp, 1) over
        (partition by user_id order by event_timestamp) as prev_event
      from raw_events
    )
)
, raw_events_w_session_id_first_last_session_flag as (
-- ts, event_name, uid, param, u_session_id, is_[1st, last]_session_event
  select
    *,
    sum(is_first_session_event) over (partition by user_id order by event_timestamp) as user_session_id,
    ifnull(lead(is_first_session_event,1) over (partition by user_id order by event_timestamp),0) as is_last_session_event
  from
    raw_events_w_first_session_flag
)
, raw_events_w_session_start_first_last_session_flag as (
-- ts, event_name, uid, param, session_ts, is_[1st, last]_session_event
  select
  *,
  min(event_timestamp) over (partition by user_id, user_session_id) as session_start
  from
    raw_events_w_session_id_first_last_session_flag
)
, collection_clicks_event as ( -- from raw_events_w_session_start_first_last_session_flag
-- ts, event_name, uid, clicked_serie, collection, session_ts
  select
    user_id,
    event_name,
    event_timestamp,
    session_start,
    cast(split((select value.string_value from unnest(event_params) where key = "location"),'-')[offset(2)] as INT64) as collection,
    cast(split((select value.string_value from unnest(event_params) where key = "oid"),'-')[offset(2)] as INT64) as clicked_serie
  from raw_events_w_session_start_first_last_session_flag
  where
    event_name in ('mr_manga_open', 'mr_manga_bookmark',
                   'mr_manga_download', 'mr_manga_chapter_read')
    and starts_with((select value.string_value from unnest(event_params) where key = "location"), 'collection_')
)
, collection_clicks_event_count as (
-- ts, event_name, uid, clicked_serie, collection, session_ts
  select
      user_id,
      session_start,
      collection,
      clicked_serie,
      sum(case when event_name = 'mr_manga_open' then 1 else 0 end) as open_count,
      sum(case when event_name = 'mr_manga_bookmark' then 1 else 0 end) as bookmark_count,
      sum(case when event_name = 'mr_manga_download' then 1 else 0 end) as download_count,
      sum(case when event_name = 'mr_manga_chapter_read' then 1 else 0 end) as read_count,
  from
    collection_clicks_event
  group by
    user_id, session_start, collection, clicked_serie
)
, session_collection_clicks as ( -- from collection_clicks_event
-- uid, collection, session_ts, [[clicked_serie, ts, event_name]]
  select
    user_id,
    session_start,
    collection,
    array_agg(struct(clicked_serie, open_count, bookmark_count, download_count, read_count)) as clicks
  from
    collection_clicks_event_count
  group by
    user_id, session_start, collection
)
-------------------
select * from collection_clicks_event_count
;
'''

USER_AUDIENCES_GENRES_LATENT = '''
--sql
with a as (select 'a')

, user_audience_genres_latent as (
  select
    *
  from
    `ml_sample.audience_user` au inner join `ml_sample.audience_genres_latent` agl on
      cast( split(audience_oid,'-')[offset(2)] as int64) = agl.audience
)

, user_audiences_genres_latent as (
  select
    uid,
    avg(latent_0) as latent_0, avg(latent_1) as latent_1, avg(latent_2) as latent_2, avg(latent_3) as latent_3, avg(latent_4) as latent_4, avg(latent_5) as latent_5, avg(latent_6) as latent_6, avg(latent_7) as latent_7, avg(latent_8) as latent_8, avg(latent_9) as latent_9, avg(latent_10) as latent_10, avg(latent_11) as latent_11, avg(latent_12) as latent_12, avg(latent_13) as latent_13, avg(latent_14) as latent_14, avg(latent_15) as latent_15, avg(latent_16) as latent_16, avg(latent_17) as latent_17, avg(latent_18) as latent_18, avg(latent_19) as latent_19, avg(latent_20) as latent_20, avg(latent_21) as latent_21, avg(latent_22) as latent_22, avg(latent_23) as latent_23, avg(latent_24) as latent_24, avg(latent_25) as latent_25, avg(latent_26) as latent_26, avg(latent_27) as latent_27, avg(latent_28) as latent_28, avg(latent_29) as latent_29, avg(latent_30) as latent_30, avg(latent_31) as latent_31, avg(latent_32) as latent_32, avg(latent_33) as latent_33, avg(latent_34) as latent_34, avg(latent_35) as latent_35, avg(latent_36) as latent_36, avg(latent_37) as latent_37, avg(latent_38) as latent_38, avg(latent_39) as latent_39, avg(latent_40) as latent_40, avg(latent_41) as latent_41, avg(latent_42) as latent_42, avg(latent_43) as latent_43, avg(latent_44) as latent_44, avg(latent_45) as latent_45, avg(latent_46) as latent_46, avg(latent_47) as latent_47, avg(latent_48) as latent_48, avg(latent_49) as latent_49
  from
    user_audience_genres_latent
  group by
    uid
)

select * from user_audiences_genres_latent
;
'''

USER_AUDIENCES_USERS_LATENT = '''
--sql
with a as (select 'a')

, user_audience_users_latent as (
  select
    *
  from
    `ml_sample.audience_user` au inner join `ml_sample.audience_users_latent` agl on
      cast( split(audience_oid,'-')[offset(2)] as int64) = agl.audience
)

, user_audiences_users_latent as (
  select
    uid,
    avg(latent_0) as latent_0, avg(latent_1) as latent_1, avg(latent_2) as latent_2, avg(latent_3) as latent_3, avg(latent_4) as latent_4, avg(latent_5) as latent_5, avg(latent_6) as latent_6, avg(latent_7) as latent_7, avg(latent_8) as latent_8, avg(latent_9) as latent_9, avg(latent_10) as latent_10, avg(latent_11) as latent_11, avg(latent_12) as latent_12, avg(latent_13) as latent_13, avg(latent_14) as latent_14, avg(latent_15) as latent_15, avg(latent_16) as latent_16, avg(latent_17) as latent_17, avg(latent_18) as latent_18, avg(latent_19) as latent_19, avg(latent_20) as latent_20, avg(latent_21) as latent_21, avg(latent_22) as latent_22, avg(latent_23) as latent_23, avg(latent_24) as latent_24, avg(latent_25) as latent_25, avg(latent_26) as latent_26, avg(latent_27) as latent_27, avg(latent_28) as latent_28, avg(latent_29) as latent_29, avg(latent_30) as latent_30, avg(latent_31) as latent_31, avg(latent_32) as latent_32, avg(latent_33) as latent_33, avg(latent_34) as latent_34, avg(latent_35) as latent_35, avg(latent_36) as latent_36, avg(latent_37) as latent_37, avg(latent_38) as latent_38, avg(latent_39) as latent_39, avg(latent_40) as latent_40, avg(latent_41) as latent_41, avg(latent_42) as latent_42, avg(latent_43) as latent_43, avg(latent_44) as latent_44, avg(latent_45) as latent_45, avg(latent_46) as latent_46, avg(latent_47) as latent_47, avg(latent_48) as latent_48, avg(latent_49) as latent_49
  from
    user_audience_users_latent
  group by
    uid
)

select * from user_audiences_users_latent
;
'''
# params = {
#     'readActivity': 'ml_sample.uid_readActivity_2019_12_01,
# }
# USER_HISTORY_LONG.format(**params)
USER_HISTORY_LONG = '''
--sql
with a as (select 1)

, readActivity_serie_latent as (
  select
    uid as user_id,
    readActivity.oid as serie,
    readActivity.readCount as read_count,
    PARSE_TIMESTAMP('%s', cast(readActivity.lastRead as string)) as last_read

  from
    `{readActivity}`,
    unnest(readActivity) as readActivity
)

, readActivity_serie_latent_weight_and_latent as (
  select
    user_id,
    serie,
    read_count as weight,
    sl.*
  from
    readActivity_serie_latent inner join `ml_sample.serie_latent` sl on
      readActivity_serie_latent.serie = cast(sl.mid as int64)
)

, user_history_latent as (
  select
    user_id,
    sum(latent_0*weight)/sum(weight) as latent_0, sum(latent_1*weight)/sum(weight) as latent_1, sum(latent_2*weight)/sum(weight) as latent_2, sum(latent_3*weight)/sum(weight) as latent_3, sum(latent_4*weight)/sum(weight) as latent_4, sum(latent_5*weight)/sum(weight) as latent_5, sum(latent_6*weight)/sum(weight) as latent_6, sum(latent_7*weight)/sum(weight) as latent_7, sum(latent_8*weight)/sum(weight) as latent_8, sum(latent_9*weight)/sum(weight) as latent_9, sum(latent_10*weight)/sum(weight) as latent_10, sum(latent_11*weight)/sum(weight) as latent_11, sum(latent_12*weight)/sum(weight) as latent_12, sum(latent_13*weight)/sum(weight) as latent_13, sum(latent_14*weight)/sum(weight) as latent_14, sum(latent_15*weight)/sum(weight) as latent_15, sum(latent_16*weight)/sum(weight) as latent_16, sum(latent_17*weight)/sum(weight) as latent_17, sum(latent_18*weight)/sum(weight) as latent_18, sum(latent_19*weight)/sum(weight) as latent_19, sum(latent_20*weight)/sum(weight) as latent_20, sum(latent_21*weight)/sum(weight) as latent_21, sum(latent_22*weight)/sum(weight) as latent_22, sum(latent_23*weight)/sum(weight) as latent_23, sum(latent_24*weight)/sum(weight) as latent_24, sum(latent_25*weight)/sum(weight) as latent_25, sum(latent_26*weight)/sum(weight) as latent_26, sum(latent_27*weight)/sum(weight) as latent_27, sum(latent_28*weight)/sum(weight) as latent_28, sum(latent_29*weight)/sum(weight) as latent_29, sum(latent_30*weight)/sum(weight) as latent_30, sum(latent_31*weight)/sum(weight) as latent_31, sum(latent_32*weight)/sum(weight) as latent_32, sum(latent_33*weight)/sum(weight) as latent_33, sum(latent_34*weight)/sum(weight) as latent_34, sum(latent_35*weight)/sum(weight) as latent_35, sum(latent_36*weight)/sum(weight) as latent_36, sum(latent_37*weight)/sum(weight) as latent_37, sum(latent_38*weight)/sum(weight) as latent_38, sum(latent_39*weight)/sum(weight) as latent_39, sum(latent_40*weight)/sum(weight) as latent_40, sum(latent_41*weight)/sum(weight) as latent_41, sum(latent_42*weight)/sum(weight) as latent_42, sum(latent_43*weight)/sum(weight) as latent_43, sum(latent_44*weight)/sum(weight) as latent_44, sum(latent_45*weight)/sum(weight) as latent_45, sum(latent_46*weight)/sum(weight) as latent_46, sum(latent_47*weight)/sum(weight) as latent_47, sum(latent_48*weight)/sum(weight) as latent_48, sum(latent_49*weight)/sum(weight) as latent_49
  from
    readActivity_serie_latent_weight_and_latent
  group by
    user_id
)

select * from user_history_latent
;
'''
# params = {
#     'session_collection_clicks': 'ml_sample.session_collection_clicks_20191201_20191218',
#     'shortterm_history_duration_day': 3,
# }
# USER_HISTORY_SHORT.format(**params)
USER_HISTORY_SHORT = '''
--sql
with a as (select 1)

, user_session as (
  select distinct
    user_id,
    session_start
  from
    `{session_collection_clicks}`
)

, session_history as (
  select
    s.user_id,
    s.session_start,
    h.session_start as history_session_start,
    h.clicked_serie,
    sum(h.open_count) as open_count,
    sum(h.bookmark_count ) as bookmark_count,
    sum(h.download_count ) as download_count,
    sum(h.read_count ) as read_count
  from
    user_session s inner join `{session_collection_clicks}` h on
      s.user_id = h.user_id and
      h.session_start < s.session_start and
      timestamp_diff(s.session_start, h.session_start, day) < {shortterm_history_duration_day}
  group by
    s.user_id,
    s.session_start,
    h.clicked_serie,
    h.session_start

)

, session_history_weight_and_latent as (
  select
    user_id,
    session_start,
    clicked_serie,
    sign(open_count) + sign((bookmark_count + download_count)) * 10 + least(read_count, 10) as weight,
    sl.*
  from
    session_history inner join `ml_sample.serie_latent` sl on
      session_history.clicked_serie = cast(sl.mid as int64)
)

, sesion_history_latent as (
  select
    user_id,
    session_start,
    sum(latent_0*weight)/sum(weight) as latent_0, sum(latent_1*weight)/sum(weight) as latent_1, sum(latent_2*weight)/sum(weight) as latent_2, sum(latent_3*weight)/sum(weight) as latent_3, sum(latent_4*weight)/sum(weight) as latent_4, sum(latent_5*weight)/sum(weight) as latent_5, sum(latent_6*weight)/sum(weight) as latent_6, sum(latent_7*weight)/sum(weight) as latent_7, sum(latent_8*weight)/sum(weight) as latent_8, sum(latent_9*weight)/sum(weight) as latent_9, sum(latent_10*weight)/sum(weight) as latent_10, sum(latent_11*weight)/sum(weight) as latent_11, sum(latent_12*weight)/sum(weight) as latent_12, sum(latent_13*weight)/sum(weight) as latent_13, sum(latent_14*weight)/sum(weight) as latent_14, sum(latent_15*weight)/sum(weight) as latent_15, sum(latent_16*weight)/sum(weight) as latent_16, sum(latent_17*weight)/sum(weight) as latent_17, sum(latent_18*weight)/sum(weight) as latent_18, sum(latent_19*weight)/sum(weight) as latent_19, sum(latent_20*weight)/sum(weight) as latent_20, sum(latent_21*weight)/sum(weight) as latent_21, sum(latent_22*weight)/sum(weight) as latent_22, sum(latent_23*weight)/sum(weight) as latent_23, sum(latent_24*weight)/sum(weight) as latent_24, sum(latent_25*weight)/sum(weight) as latent_25, sum(latent_26*weight)/sum(weight) as latent_26, sum(latent_27*weight)/sum(weight) as latent_27, sum(latent_28*weight)/sum(weight) as latent_28, sum(latent_29*weight)/sum(weight) as latent_29, sum(latent_30*weight)/sum(weight) as latent_30, sum(latent_31*weight)/sum(weight) as latent_31, sum(latent_32*weight)/sum(weight) as latent_32, sum(latent_33*weight)/sum(weight) as latent_33, sum(latent_34*weight)/sum(weight) as latent_34, sum(latent_35*weight)/sum(weight) as latent_35, sum(latent_36*weight)/sum(weight) as latent_36, sum(latent_37*weight)/sum(weight) as latent_37, sum(latent_38*weight)/sum(weight) as latent_38, sum(latent_39*weight)/sum(weight) as latent_39, sum(latent_40*weight)/sum(weight) as latent_40, sum(latent_41*weight)/sum(weight) as latent_41, sum(latent_42*weight)/sum(weight) as latent_42, sum(latent_43*weight)/sum(weight) as latent_43, sum(latent_44*weight)/sum(weight) as latent_44, sum(latent_45*weight)/sum(weight) as latent_45, sum(latent_46*weight)/sum(weight) as latent_46, sum(latent_47*weight)/sum(weight) as latent_47, sum(latent_48*weight)/sum(weight) as latent_48, sum(latent_49*weight)/sum(weight) as latent_49
  from
    session_history_weight_and_latent
  group by
    user_id, session_start
)

select * from sesion_history_latent
;
'''
# params = {
#     'session_collection_clicks': 'ml_sample.session_collection_clicks_20191201_20191218',
#     'readActivity': 'ml_sample.uid_readActivity_2019_12_01',
# }
# USER_SERIE_HISTORY.format(**params)
USER_SERIE_HISTORY = '''
--sql
with a as (select 1)
, user_session_serie as (
  select distinct
    user_id,
    session_start,
    clicked_serie
  from
    `{session_collection_clicks}`
)

, prev_time_by_session as (
  select
    s.user_id,
    s.session_start,
    s.clicked_serie,
    max(h.session_start) as prev_time,
  from
    user_session_serie s inner join `{session_collection_clicks}` h on
      s.user_id = h.user_id and
      s.clicked_serie = h.clicked_serie and
      s.session_start > h.session_start
  group by
    s.user_id,
    s.session_start,
    s.clicked_serie
)

# assume readActivity's max(date) < events' min(date)
, readActivity as (
  select
    uid as user_id,
    readActivity.oid as serie,
    PARSE_TIMESTAMP('%s', cast(readActivity.lastRead as string)) as last_read
  from
    `{readActivity}`,
    unnest(readActivity) as readActivity
)

, prev_time_by_readActivity as (
  select
    s.user_id,
    s.session_start ,
    s.clicked_serie,
    max(h.last_read) as prev_time,
  from
    user_session_serie s inner join readActivity h on
      s.user_id = h.user_id and
      s.clicked_serie = h.serie
  group by
    s.user_id,
    s.clicked_serie,
    s.session_start
)

, prev_time_all as (
  select
    user_id,
    session_start,
    clicked_serie,
    prev_time
  from
    prev_time_by_session ss union all (select * from prev_time_by_readActivity)
)

, prev_time as (
  select
    user_id,
    session_start,
    clicked_serie,
    max(prev_time) as prev_time
  from
    prev_time_all
  group by
    user_id,
    session_start,
    clicked_serie
)

select * from prev_time
;
'''

if __name__ == "__main__":
    from datetime import datetime

    # params = {
    #     'date_begin': datetime(2019, 12, 9),
    #     'date_end': datetime(2019, 12, 18),
    #     'session_time_out': 60
    # }
    # print(SESSION_COLLECTION_CLICKS.format(**params))
    # print('__________________________________________________________')

    params = {
        'session_collection_clicks': 'ml_sample.session_collection_clicks_20191128_20191207',
        'user_history_long': 'ml_sample.user_history_long_20191201',
        'user_history_short': 'ml_sample.user_history_short_20191128_20191207',
        'user_serie_history': 'ml_sample.user_serie_history__20191128_20191207',
    }
    print(DATA_ENSEMBLE.format(**params))
    print('__________________________________________________________')

    # params = {
    #     'readActivity': 'ml_sample.uid_readActivity_2019_12_01',
    # }
    # print(USER_HISTORY_LONG.format(**params))
    # print('__________________________________________________________')

    # params = {
    #     'session_collection_clicks': 'ml_sample.session_collection_clicks_20191201_20191218',
    #     'shortterm_history_duration_day': 3,
    # }
    # print(USER_HISTORY_SHORT.format(**params))
    # print('__________________________________________________________')

    # params = {
    #     'session_collection_clicks': 'ml_sample.session_collection_clicks_20191201_20191218',
    #     'readActivity': 'ml_sample.uid_readActivity_2019_12_01',
    # }
    # print(USER_SERIE_HISTORY.format(**params))
