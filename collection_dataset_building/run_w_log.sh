#!/bin/bash

# eg: ./run_w_log.sh -c "print('a')"

LOGDIR="logs"
TIMESTAMP=$(date +%Y%m%d_%H_%M)
CMD="${1##*/}"
LOGFILE="${LOGDIR}/${TIMESTAMP}_${CMD}.log"
echo $LOGFILE
touch $LOGFILE
echo "Loggings for '$@' at $TIMESTAMP" >s> $LOGFILE
echo "===============================" >> $LOGFILE

echo "Saving logs to $LOGFILE"
echo "Executing $@"

export PYTHONUNBUFFERED=1
stdbuf -o L -e L python $@ 2>&1 | tee -a $LOGFILE
