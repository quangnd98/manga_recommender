#!/usr/bin/env bash

export GOOGLE_APPLICATION_CREDENTIALS="/home/$(whoami)/.config/gcloud/legacy_credentials/quang.nguyen@nabstudio.com/adc.json"
export GCLOUD_PROJECT="manga-rock-9cfc9"

./run_w_log.sh build_dataset.py \
                --date_begin 20200101 \
                --date_end 20200103 \
                --user_history_long_date 20191201 \
                --session_time_out 60 \
                --shortterm_history_duration_day 3 \
                \
                --qid 1 \
                \
                --label multi \
                --three_labels_mode 0 \
                --single_click_mode 0 \
                \
                --output_dir ../data_bk/cate \
                --project_id manga-rock-9cfc9 \
                --dataset ml_sample \
                --cache 1
