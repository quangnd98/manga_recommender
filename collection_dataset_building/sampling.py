#!/usr/bin/env python
import numpy as np

from numpy import ndarray
from xgboost import DMatrix
from sklearn.model_selection import train_test_split


def _load(svmlight_path, X_c_npy_path, X_k_npy_path, cache=True):
    cached_path = f'{svmlight_path}#{svmlight_path}.cache'
    if svmlight_path is not None:
        d = DMatrix(cached_path)
    else:
        d = None
    X_c = np.load(X_c_npy_path)
    X_k = np.load(X_k_npy_path)

    return d, X_c, X_k


def _split(d: DMatrix, X_c: ndarray, X_k: ndarray, p: float, random_state=None):
    if d is not None:
        assert d.num_row() == len(X_c)
    assert len(X_c) == len(X_k)

    indices = np.arange(len(X_c))
    indices, _ = train_test_split(indices, train_size=p, stratify=X_c)

    if d is not None:
        new_d = d.slice(indices)
    else:
        new_d = None
    new_X_c = X_c.take(indices)
    new_X_k = X_k.take(indices)

    return new_d, new_X_c, new_X_k, indices


def _export(d: DMatrix, X_c: ndarray, X_k: ndarray, prefix):
    if d is not None:
        d.save_binary(f'{prefix}.DMatrix.bin')
    X_c.dump(f'{prefix}.X_c.npy')
    X_k.dump(f'{prefix}.X_k.npy')


def _split_d_txt(dpath, outpath, indices):
    _id = set(indices)
    with open(dpath, 'r') as fi, open(outpath, 'w') as fo:
        for i, line in enumerate(fi):
            if i%500000 == 0:
                print(i)
            if i in _id:
                fo.write(line)


if __name__ == "__main__":
    import argparse

    def str2bool(v):
        if isinstance(v, bool):
            return v
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    parser = argparse.ArgumentParser(
        description='Generate within-collection ranking dataset')

    parser.add_argument('--svmlight_path', type=str, required=True)
    parser.add_argument('--X_c_npy_path', type=str, required=True)
    parser.add_argument('--X_k_npy_path', type=str, required=True)
    parser.add_argument('--new_prefix', type=str, required=True)
    parser.add_argument('--p', type=float, default=0.1)
    parser.add_argument('--cache', type=str2bool, default=True)

    args = parser.parse_args()

    print(args)

    svmlight_path, X_c_npy_path, X_k_npy_path = args.svmlight_path, args.X_c_npy_path, args.X_k_npy_path
    p, new_prefix = args.p, args.new_prefix

    _, X_c, X_k = _load(None, X_c_npy_path, X_k_npy_path)

    _, new_X_c, new_X_k, indices = _split(None, X_c, X_k, p)

    _export(None, new_X_c, new_X_k, new_prefix)

    _split_d_txt(svmlight_path, f'{new_prefix}.svmlight', indices)

    print('Finished')

# ./run_w_log.sh dataset_building/sampling.py --svmlight_path data/_single_click_20191201_20191231.svmlight --X_c_npy_path data/_single_click_20191201_20191231.X_c.npy --X_k_npy_path data/_single_click_20191201_20191231.X_k.npy --p 0.5 --new_prefix data/_single_click_20191201_20191231_resampled_0.5
