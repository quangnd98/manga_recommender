"""Modified by Quang Nguyen for progress bar, multithreads dumping, and query_id only reading

Multithreads dumping can caused RAM exhausting
"""

"""This module implements a loader and dumper for the svmlight format

This format is a text-based format, with one sample per line. It does
not store zero valued features hence is suitable for sparse dataset.

The first element of each line can be used to store a target variable to
predict.

This format is used as the default format for both svmlight and the
libsvm command line programs.
"""

# Authors: Mathieu Blondel <mathieu@mblondel.org>
#          Lars Buitinck
#          Olivier Grisel <olivier.grisel@ensta.org>
# License: BSD 3 clause

import io
import os.path
from contextlib import closing

import numpy as np
import scipy.sparse as sp
from joblib import Parallel, delayed
from sklearn.datasets._svmlight_format_fast import _load_svmlight_file
from sklearn.utils import check_array
from tqdm import tqdm


def _gen_open(f):
    if isinstance(f, int):  # file descriptor
        return io.open(f, "rb", closefd=False)
    elif not isinstance(f, str):
        raise TypeError("expected {str, int, file-like}, got %s" % type(f))

    _, ext = os.path.splitext(f)
    if ext == ".gz":
        import gzip
        return gzip.open(f, "rb")
    elif ext == ".bz2":
        from bz2 import BZ2File
        return BZ2File(f, "rb")
    else:
        return open(f, "rb")


def _open_and_load_query(f, dtype, multilabel, zero_based, query_id,
                   offset=0, length=-1):
    if hasattr(f, "read"):
        actual_dtype, data, ind, indptr, labels, query = \
            _load_svmlight_file(f, dtype, multilabel, zero_based, query_id,
                                offset, length)
    else:
        with closing(_gen_open(f)) as f:
            actual_dtype, data, ind, indptr, labels, query = \
                _load_svmlight_file(f, dtype, multilabel, zero_based, query_id,
                                    offset, length)

    # convert from array.array, give data the right dtype
    query = np.frombuffer(query, np.int64)

    return query

def load_query_id(file, n_features=None, dtype=np.float64,
                        multilabel=False, zero_based="auto",
                        offset=0, length=-1):
    if (offset != 0 or length > 0) and zero_based == "auto":
        # disable heuristic search to avoid getting inconsistent results on
        # different segments of the file
        zero_based = True

    if (offset != 0 or length > 0) and n_features is None:
        raise ValueError(
            "n_features is required when offset or length is specified.")

    query_values = _open_and_load_query(file, dtype, multilabel, 
                                        bool(zero_based), query_id=True, 
                                        offset=offset, length=length
                                        )

    return query_values

def ___dump_svmlight_parallel(i, X, y, multilabel, one_based,
                              query_id, cost, X_is_sp, y_is_sp,
                              line_pattern, value_pattern, label_pattern,
                              row_id, row_id_begin):
    if X_is_sp:
        span = slice(X.indptr[i], X.indptr[i + 1])
        row = zip(X.indices[span], X.data[span])
    else:
        nz = X[i] != 0
        row = zip(np.where(nz)[0], X[i, nz])

    s = " ".join(value_pattern % (j + one_based, x) for j, x in row)

    if multilabel:
        if y_is_sp:
            nz_labels = y[i].nonzero()[1]
        else:
            nz_labels = np.where(y[i] != 0)[0]
        labels_str = ",".join(label_pattern % j for j in nz_labels)
    else:
        if y_is_sp:
            labels_str = label_pattern % y.data[i]
        else:
            labels_str = label_pattern % y[i]

    extra_fields = []

    if query_id is not None:
        extra_fields.append(query_id[i])

    if cost is not None:
        extra_fields.append(cost[i])

    if row_id:
            feat = (i + row_id_begin, labels_str, *extra_fields, s)
    else:
        feat = (labels_str, *extra_fields, s)

    line = (line_pattern % feat).encode('ascii')

    return line


def _dump_svmlight_parallel(its, X, y, f, multilabel, one_based,
                            query_id, cost, X_is_sp, y_is_sp,
                            line_pattern, value_pattern, label_pattern,
                            verbose, row_id, row_id_begin):
    verbose = 1 if verbose else 0

    if verbose: print(f'Total job = {len(its)}')
    lines = Parallel(n_jobs=-5, verbose=verbose)(delayed(___dump_svmlight_parallel)
                                                 (i, X, y, multilabel, one_based,
                                                  query_id, cost, X_is_sp, y_is_sp,
                                                  line_pattern, value_pattern, label_pattern,
                                                  row_id, row_id_begin) for i in its)

    f.writelines(lines)


def _dump_svm_light_single(its, X, y, f, multilabel, one_based,
                           query_id, cost, X_is_sp, y_is_sp,
                           line_pattern, value_pattern, label_pattern,
                           row_id, row_id_begin):
    for i in its:
        if X_is_sp:
            span = slice(X.indptr[i], X.indptr[i + 1])
            row = zip(X.indices[span], X.data[span])
        else:
            nz = X[i] != 0
            row = zip(np.where(nz)[0], X[i, nz])

        s = " ".join(value_pattern % (j + one_based, x) for j, x in row)

        if multilabel:
            if y_is_sp:
                nz_labels = y[i].nonzero()[1]
            else:
                nz_labels = np.where(y[i] != 0)[0]
            labels_str = ",".join(label_pattern % j for j in nz_labels)
        else:
            if y_is_sp:
                labels_str = label_pattern % y.data[i]
            else:
                labels_str = label_pattern % y[i]

        extra_fields = []

        if query_id is not None:
            extra_fields.append(query_id[i])

        if cost is not None:
            extra_fields.append(cost[i])

        if row_id:
            feat = (i + row_id_begin, labels_str, *extra_fields, s)
        else:
            feat = (labels_str, *extra_fields, s)

        f.write((line_pattern % feat).encode('ascii'))


def _dump_svmlight(X, y, f, multilabel, one_based, comment, query_id, cost, verbose, parallel, first, row_id, row_id_begin):
    X_is_sp = int(hasattr(X, "tocsr"))
    y_is_sp = int(hasattr(y, "tocsr"))
    if X.dtype.kind == 'i':
        value_pattern = "%d:%d"
    elif X.dtype.kind == 'f':
        value_pattern = "%d:%.16f"
    else:
        value_pattern = "%d:%s"

    if y.dtype.kind == 'i':
        label_pattern = "%d"
    else:
        label_pattern = "%.16f"

    line_pattern = "%s"
    if query_id is not None:
        line_pattern += " qid:%d"
    if cost is not None:
        line_pattern += " cost:%.16f"
    line_pattern += " %s\n"

    if row_id:
        line_pattern = '%d ' + line_pattern

    if comment and first:
        f.write(("# Column indices are %s-based\n"
                 % ["zero", "one"][one_based]).encode())

        f.write(b"#\n")
        f.writelines(b"# %s\n" % line for line in comment.splitlines())

    its = range(X.shape[0])


    if parallel:
        _dump_svmlight_parallel(its, X, y, f, multilabel, one_based,
                                query_id, cost, X_is_sp, y_is_sp,
                                line_pattern, value_pattern, label_pattern,
                                verbose, row_id, row_id_begin)
    else:
        if verbose:
            its = tqdm(range(X.shape[0]), total=X.shape[0])
        _dump_svm_light_single(its, X, y, f, multilabel, one_based,
                               query_id, cost, X_is_sp, y_is_sp,
                               line_pattern, value_pattern, label_pattern, row_id,
                               row_id_begin)


def dump_svmlight_file(X, y, f,  zero_based=True, comment=None, query_id=None,
                       multilabel=False, cost=None, verbose=True, parallel=False,
                       append=False, first=True, row_id=False, row_id_begin=None):
    """Dump the dataset in svmlight / libsvm file format.

    This format is a text-based format, with one sample per line. It does
    not store zero valued features hence is suitable for sparse dataset.

    The first element of each line can be used to store a target variable
    to predict.

    Parameters
    ----------
    X : {array-like, sparse matrix} of shape (n_samples, n_features)
        Training vectors, where n_samples is the number of samples and
        n_features is the number of features.

    y : {array-like, sparse matrix}, shape = [n_samples (, n_labels)]
        Target values. Class labels must be an
        integer or float, or array-like objects of integer or float for
        multilabel classifications.

    f : string or file-like in binary mode
        If string, specifies the path that will contain the data.
        If file-like, data will be written to f. f should be opened in binary
        mode.

    zero_based : boolean, optional
        Whether column indices should be written zero-based (True) or one-based
        (False).

    comment : string, optional
        Comment to insert at the top of the file. This should be either a
        Unicode string, which will be encoded as UTF-8, or an ASCII byte
        string.
        If a comment is given, then it will be preceded by one that identifies
        the file as having been dumped by scikit-learn. Note that not all
        tools grok comments in SVMlight files.

    query_id : array-like of shape (n_samples,)
        Array containing pairwise preference constraints (qid in svmlight
        format).

    multilabel : boolean, optional
        Samples may have several labels each (see
        https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multilabel.html)

        .. versionadded:: 0.17
           parameter *multilabel* to support multilabel datasets.
    """
    if comment is not None:
        # Convert comment string to list of lines in UTF-8.
        # If a byte string is passed, then check whether it's ASCII;
        # if a user wants to get fancy, they'll have to decode themselves.
        # Avoid mention of str and unicode types for Python 3.x compat.
        if isinstance(comment, bytes):
            comment.decode("ascii")  # just for the exception
        else:
            comment = comment.encode("utf-8")
        if b"\0" in comment:
            raise ValueError("comment string contains NUL byte")

    yval = check_array(y, accept_sparse='csr', ensure_2d=False)
    if sp.issparse(yval):
        if yval.shape[1] != 1 and not multilabel:
            raise ValueError("expected y of shape (n_samples, 1),"
                             " got %r" % (yval.shape,))
    else:
        if yval.ndim != 1 and not multilabel:
            raise ValueError("expected y of shape (n_samples,), got %r"
                             % (yval.shape,))

    Xval = check_array(X, accept_sparse='csr',)
    if Xval.shape[0] != yval.shape[0]:
        raise ValueError(
            "X.shape[0] and y.shape[0] should be the same, got"
            " %r and %r instead." % (Xval.shape[0], yval.shape[0])
        )

    # We had some issues with CSR matrices with unsorted indices (e.g. #1501),
    # so sort them here, but first make sure we don't modify the user's X.
    # TODO We can do this cheaper; sorted_indices copies the whole matrix.
    if yval is y and hasattr(yval, "sorted_indices"):
        y = yval.sorted_indices()
    else:
        y = yval
        if hasattr(y, "sort_indices"):
            y.sort_indices()

    if Xval is X and hasattr(Xval, "sorted_indices"):
        X = Xval.sorted_indices()
    else:
        X = Xval
        if hasattr(X, "sort_indices"):
            X.sort_indices()

    if query_id is not None:
        query_id = np.asarray(query_id)
        if query_id.shape[0] != y.shape[0]:
            raise ValueError("expected query_id of shape (n_samples,), got %r"
                             % (query_id.shape,))

    if cost is not None:
        cost = np.asarray(cost)
        if cost.shape[0] != y.shape[0]:
            raise ValueError("expected cost of shape (n_samples,), got %r"
                             % (query_id.shape,))

    one_based = not zero_based

    open_mode = "ab" if append else "wb"
    first = (not append) or (append and first)

    if hasattr(f, "write"):
        _dump_svmlight(X, y, f, multilabel, one_based, comment,
                       query_id, cost, verbose, parallel, first, row_id, row_id_begin)
    else:
        with open(f, open_mode) as f:
            _dump_svmlight(X, y, f, multilabel, one_based,
                           comment, query_id, cost, verbose, parallel, first, row_id, row_id_begin)
