#!/usr/bin/env python

import os
import fnmatch
import numpy as np
import google.auth
from datetime import datetime, timedelta

import pandas as pd
from google.cloud import bigquery, bigquery_storage_v1beta1, storage
from google.cloud.exceptions import NotFound
from google.api_core.exceptions import PermissionDenied

import _query_template as query_template
from _remove_outliers import remove_outlier


def is_table_exists(table_id, client):
    try:
        client.get_table(table_id)  # Make an API request.
        return True
    except NotFound:
        return False


def is_blob_exists(bucket_name, filename, storage_client):
    bucket = storage_client.bucket(bucket_name)
    match = 0
    for blob in bucket.list_blobs():
        if fnmatch.fnmatch(blob.name, filename):
            match += 1
    return match


def user_latent(dataset, client):
    table = f'{dataset}.user_latent'
    if not is_table_exists(table, client=client):
        raise Exception(f'{table} 404')
    else:
        print(f'Table {dataset}.user_latent exists, proceeding...')
    return table


def serie_latent(dataset, client):
    table = f'{dataset}.serie_latent'
    if not is_table_exists(table, client=client):
        raise Exception(f'{table} 404')
    else:
        print(f'Table {dataset}.serie_latent exists, proceeding...')
    return table


def collection_genres_latent(dataset, client):
    table = f'{dataset}.collection_genres_latent'
    if not is_table_exists(table, client=client):
        raise Exception(f'{table} 404')
    else:
        print(f'Table {dataset}.collection_genres_latent exists, proceeding...')
    return table


def collection_series_latent(dataset, client):
    table = f'{dataset}.collection_series_latent'
    if not is_table_exists(table, client=client):
        raise Exception(f'{table} 404')
    else:
        print(f'Table {dataset}.collection_series_latent exists, proceeding...')
    return table


def user_audiences_users_latent(dataset, client):
    table = f'{dataset}.user_audiences_users_latent'
    if not is_table_exists(table, client=client):
        raise Exception(f'{table} 404')
    else:
        print(
            f'Table {dataset}.user_audiences_users_latent exists, proceeding...')
    return table


def user_audiences_genres_latent(dataset, client):
    table = f'{dataset}.user_audiences_genres_latent'
    if not is_table_exists(table, client=client):
        raise Exception(f'{table} 404')
    else:
        print(
            f'Table {dataset}.user_audiences_genres_latent exists, proceeding...')
    return table


def session_collection_clicks(session_collection_clicks_date_begin, session_collection_clicks_date_end, session_time_out, project, dataset, client):
    table = f'{dataset}.session_collection_clicks_{session_collection_clicks_date_begin:%Y%m%d}_{session_collection_clicks_date_end:%Y%m%d}'

    if not is_table_exists(table, client=client):
        from joblib import Parallel, delayed
        print(f'Table {table} not found, attemping to create...')

        print(
            f'Checking from android_event_new_{session_collection_clicks_date_begin:%Y%m%d} to android_event_new_{session_collection_clicks_date_end:%Y%m%d}')
        day_count = (session_collection_clicks_date_end -
                     session_collection_clicks_date_begin).days
        dates = [session_collection_clicks_date_begin +
                 timedelta(n) for n in range(day_count+1)]
        Parallel(n_jobs=-5, verbose=1)(delayed(android_event)(d)
                                       for d in dates)

        # sanity check
        for d in dates:
            event_table = f'ml_sample.android_event_new_{d:%Y%m%d}'
            if not is_table_exists(event_table, client=client):
                raise Exception(
                    f'Sanity check failed :( Table {event_table} 404')

        # create table
        print(f'Creating {table}...')
        job_config = bigquery.QueryJobConfig(destination=f'{project}.{table}')
        params = {
            'date_begin': session_collection_clicks_date_begin,
            'date_end': session_collection_clicks_date_end,
            'session_time_out': session_time_out
        }
        query = query_template.SESSION_COLLECTION_CLICKS.format(**params)
        print(f'Commit query: {query}')
        query_job = client.query(query, job_config=job_config)
        print('Waiting for the job to complete...')
        query_job.result()

        # sanity check
        if not is_table_exists(table, client=client):
            raise Exception(f'Sanity check failed :( Table {table} 404')
        print(f'Table {table} created')

    else:
        print(f'Table {table} exists, proceeding...')
    return table


def android_event(d, client=None, force=False):
    table = f'ml_sample.android_event_new_{d:%Y%m%d}'

    if client is None:
        client = bigquery.Client()

    if force or (not is_table_exists(table, client=client)):
        print(f'Table {table} not found, attemping to create...')

        import kfp
        kfpclient = kfp.Client(
            host='192.168.102.23:31380/pipeline',
        )
        run = kfpclient.run_pipeline(
            experiment_id='1f6dff08-b88b-4ffd-b7c8-f9c4263bea42',
            job_name=f'prepare_data_{d:%Y%m%d}',
            pipeline_id='31aa9080-8cbe-45bd-a537-c602aeda34c4',
            params={
                'data-date': f'{d:%Y%m%d}',
                'project-id': 'manga-rock-9cfc9',
                'dataset-id': 'ml_sample',
                'dataset-location': 'US'
            }
        )
        kfpclient.wait_for_run_completion(run.id, 300)

        if not is_table_exists(table, client=client):
            raise Exception(
                f'Kubeflow run for {table} has finished but the table is not found. Please check and rerun.')
        print(f'Table {table} created')
    else:
        print(f'Table {table} exists, proceeding...')

    return table


def user_history_long(user_history_long_date, project, dataset, client):
    table = f'{dataset}.user_history_long_{user_history_long_date:%Y%m%d}'

    if not is_table_exists(table, client=client):
        # create table
        print(f'Creating {table}...')
        job_config = bigquery.QueryJobConfig(destination=f'{project}.{table}')
        params = {
            'readActivity': f'ml_sample.uid_readActivity_{user_history_long_date:%Y_%m_%d}',
        }
        query = query_template.USER_HISTORY_LONG.format(**params)
        print(f'Commit query: {query}')
        query_job = client.query(query, job_config=job_config)
        print('Waiting for the job to complete...')
        query_job.result()

        # sanity check
        if not is_table_exists(table, client=client):
            raise Exception(f'Sanity check failed :( Table {table} 404')
        print(f'Table {table} created')
    else:
        print(f'Table {table} exists, proceeding...')
    return table


def user_history_short(session_collection_clicks_date_begin, session_collection_clicks_date_end, shortterm_history_duration_day, project, dataset, client):
    table = f'{dataset}.user_history_short_{session_collection_clicks_date_begin:%Y%m%d}_{session_collection_clicks_date_end:%Y%m%d}'

    if not is_table_exists(table, client=client):
        # create table
        print(f'Creating {table}...')
        job_config = bigquery.QueryJobConfig(destination=f'{project}.{table}')
        params = {
            'session_collection_clicks': f'ml_sample.session_collection_clicks_{session_collection_clicks_date_begin:%Y%m%d}_{session_collection_clicks_date_end:%Y%m%d}',
            'shortterm_history_duration_day': shortterm_history_duration_day,
        }
        query = query_template.USER_HISTORY_SHORT.format(**params)
        print(f'Commit query: {query}')
        query_job = client.query(query, job_config=job_config)
        print('Waiting for the job to complete...')
        query_job.result()

        # sanity check
        if not is_table_exists(table, client=client):
            raise Exception(f'Sanity check failed :( Table {table} 404')
        print(f'Table {table} created')
    else:
        print(f'Table {table} exists, proceeding...')
    return table


def user_serie_history(session_collection_clicks_date_begin, session_collection_clicks_date_end, user_history_long_date, project, dataset, client):
    table = f'{dataset}.user_serie_history_{session_collection_clicks_date_begin:%Y%m%d}_{session_collection_clicks_date_end:%Y%m%d}'

    if not is_table_exists(table, client=client):
        # create table
        print(f'Creating {table}...')
        job_config = bigquery.QueryJobConfig(destination=f'{project}.{table}')
        params = {
            'session_collection_clicks': f'ml_sample.session_collection_clicks_{session_collection_clicks_date_begin:%Y%m%d}_{session_collection_clicks_date_end:%Y%m%d}',
            'readActivity': f'ml_sample.uid_readActivity_{user_history_long_date:%Y_%m_%d}',
        }
        query = query_template.USER_SERIE_HISTORY.format(**params)
        print(f'Commit query: {query}')
        query_job = client.query(query, job_config=job_config)
        print('Wait for the job to complete...')
        query_job.result()

        # sanity check
        if not is_table_exists(table, client=client):
            raise Exception(f'Sanity check failed :( Table {table} 404')
        print(f'Table {table} created')
    else:
        print(f'Table {table} exists, proceeding...')
    return table


def ensemble(date_begin,
             date_end,
             shortterm_history_duration_day,
             user_history_long_date,
             session_time_out,
             project,
             dataset,
             client,
             storage_client,
             outdir='.',
             bqstorage_client=None,
             label='both',
             mode_3_label=False,
             single_click_mode=False,
             cache=True,
             qid_mode=False
             ):

    shortterm_history_buffer = timedelta(days=shortterm_history_duration_day)
    session_collection_clicks_date_begin = date_begin - shortterm_history_buffer
    session_collection_clicks_date_end = date_end

    # make sure prequsite tables exist
    # static prequesites
    static_prequesites = [
        user_latent,
        serie_latent,
        collection_genres_latent,
        collection_series_latent,
        user_audiences_genres_latent,
        user_audiences_users_latent
    ]
    for p in static_prequesites:
        p(dataset=dataset, client=client)

    # dynamic prequesites
    session_collection_clicks_name = session_collection_clicks(
        session_collection_clicks_date_begin=session_collection_clicks_date_begin,
        session_collection_clicks_date_end=session_collection_clicks_date_end,
        session_time_out=session_time_out,
        project=project,
        dataset=dataset,
        client=client
    )
    user_history_long_name = user_history_long(
        user_history_long_date=user_history_long_date,
        project=project,
        dataset=dataset,
        client=client
    )
    user_history_short_name = user_history_short(
        session_collection_clicks_date_begin=session_collection_clicks_date_begin,
        session_collection_clicks_date_end=session_collection_clicks_date_end,
        shortterm_history_duration_day=shortterm_history_duration_day,
        project=project,
        dataset=dataset,
        client=client
    )
    user_serie_history_name = user_serie_history(
        session_collection_clicks_date_begin=session_collection_clicks_date_begin,
        session_collection_clicks_date_end=session_collection_clicks_date_end,
        user_history_long_date=user_history_long_date,
        project=project,
        dataset=dataset,
        client=client
    )

    name = f'DATA_ENSEMBLE{("_SINGLE_CLICK" if single_click_mode else "")}_with_categorical_{session_collection_clicks_name}_{user_history_long_name}_{user_history_short_name}_{user_serie_history_name}'
    table = f'{name.replace(".","_")}'
    gs_bucket = 'nabstudio'
    gs_json_cache = f'Datasets/Temp/{name}_*.avro'
    # gs_json_cache = f'Datasets/Temp/{name}_*.json'
    df_cache_pkl_path = os.path.join(
        outdir, f'{name}.pkl')

    if not is_blob_exists(gs_bucket, gs_json_cache, storage_client):
        if not is_table_exists(f'{dataset}.{table}', client):
            print(f'Creating {table}...')

            # composing query
            params = {
                'session_collection_clicks': session_collection_clicks_name,
                'user_history_long': user_history_long_name,
                'user_history_short': user_history_short_name,
                'user_serie_history': user_serie_history_name,
            }
            if single_click_mode:
                query = query_template.DATA_ENSEMBLE_SINGLE_CLICK.format(
                    **params)
            else:
                query = query_template.DATA_ENSEMBLE.format(**params)


            print(f'Commit query: {query}')
            job_config = bigquery.QueryJobConfig(
                destination=f'{project}.{dataset}.{table}')
            query_job = client.query(query, job_config=job_config)

            print('Wait for the job to complete...')
            query_job.result()
            print("Query results loaded to the table {}".format(table))
        else:
            print(f'Table {dataset}.{table} already exists')

        print(f'Exporting table {table} to gcs')
        destination_uri = "gs://{}/{}".format(gs_bucket, gs_json_cache)
        dataset_ref = client.dataset(dataset, project=project)
        table_ref = dataset_ref.table(table)

        job_config = bigquery.job.ExtractJobConfig(destination_format='AVRO', use_avro_logical_types=True)
        extract_job = client.extract_table(
            table_ref,
            destination_uri,
            # Location must match that of the source table.
            location="US",
            job_config=job_config,
        )  # API request
        extract_job.result()  # Waits for job to complete.

        print(
            "Exported {}:{}.{} to {}".format(
                project, dataset, table, destination_uri)
        )
    else:
        print(f'Gcs files already exist')

    print('Downloading files to local...')
    bucket = storage_client.bucket(gs_bucket)
    file_names = []

    for blob in bucket.list_blobs():
        if fnmatch.fnmatch(blob.name, gs_json_cache):
            file_name = os.path.basename(blob.name)
            file_path = os.path.join(outdir, file_name)
            feather_path = file_path + '.feather'
            file_names.append(file_name)
            if (not os.path.exists(file_path)) and (not os.path.exists(feather_path)):
                with open(file_path, 'wb') as fh:
                    storage_client.download_blob_to_file(blob, fh)
            else:
                print(f'{file_path} already exist')

    def export_to_feather(file_name):
        file_path = os.path.join(outdir, file_name)
        feather_path = file_path + '.feather'
        if not os.path.exists(feather_path):
            print(f'Converting {file_path}...')
            df = pdx.read_avro(file_path)
            df.to_feather(file_path + '.feather')
    
    if gs_json_cache.endswith('.avro'):
        import pandavro as pdx
        from joblib import Parallel, delayed

        Parallel(n_jobs=1, verbose=1)(delayed(export_to_feather)(i) for i in file_names)


    print('Merging into (svmlight, X_c, X_k)...')
    basename = f'{("_single_click" if single_click_mode else "")}_{date_begin:%Y%m%d}_{date_end:%Y%m%d}'

    svmlight_name = basename + '.svmlight'
    svmlight_path = os.path.join(outdir, svmlight_name)

    X_c_name = basename + '.X_c.npy'
    X_c_path = os.path.join(outdir, X_c_name)

    X_k_name = basename + '.X_k.npy'
    X_k_path = os.path.join(outdir, X_k_name)

    if qid_mode:
        qid_name = basename + '.qid.npy'
        qid_path = os.path.join(outdir, qid_name)

    if os.path.exists(svmlight_path) and not qid_mode:
        print(f'{svmlight_path} exists, exiting...')
    else:
        if not os.path.exists(svmlight_path):
            import pandavro as pdx
            from _svmlight_format_io import dump_svmlight_file

            file_names.sort()
            X_c = []
            X_k = []
            qid = []
            qid_columns = 'qid'
            y_columns = 'label'

            row_id_begin = 0
            for i, file_name in enumerate(file_names):
                print(f'Processing {file_name}...')
                file_path = os.path.join(outdir, file_name)
                if os.path.exists(file_path + '.feather'):
                    df = pd.read_feather(file_path + '.feather')
                else:
                    df = pdx.read_avro(file_path)

                # print('Cutting off shortterm memory buffer days...')
                df['datetime_date'] = df.session_start.apply(lambda x: x.date())
                df = df[(date_begin.date() <= df.datetime_date)
                        & (df.datetime_date <= date_end.date())]
                del df['datetime_date']

                # print('Calculating user serie history...')
                df['user_serie_history'] = (
                    df.session_start - df.prev_time.fillna(pd.NaT)).dt.days.fillna(-1)

                # df = df.iloc[:200]

                # Composing 
                X_columns = np.concatenate([
                    ['user_categorical_id', 'serie_categorical_id'],
                    [f'user_latent_{i}' for i in range(50)],
                    [f'user_history_short_latent_{i}' for i in range(50)],
                    [f'user_history_long_latent_{i}' for i in range(50)],
                    [f'user_audiences_users_latent_{i}' for i in range(50)],
                    [f'user_audiences_genres_latent_{i}' for i in range(50)],
                    [f'collection_genres_latent_{i}' for i in range(50)],
                    [f'collection_series_latent_{i}' for i in range(50)],
                    [f'serie_latent_{i}' for i in range(50)],
                    ['user_serie_history'],
                    # ['user_id', 'session_start', 'collection', 'serie', 'serie_order'],
                ])
                X = df[X_columns]
                X = X.fillna(0).values

                if label == 'binary':
                    y = (df[y_columns] > 0).astype(int)
                elif label == 'multi':
                    # merge 2 and 3 to 2
                    if mode_3_label:
                        df[y_columns][df[y_columns] == 3] = 2
                    y = df[y_columns].values
                else:
                    raise ValueError(f'Unknown label type {label}')

                _qid = None

                if qid_mode:
                    _qid = df[qid_columns].values

                row_id = True if qid_mode else False

                X_c.append(y)
                X_k.append(df.serie_order.values)
                if qid_mode: qid.append(_qid)

                print(f'Dumping {file_name}...')
                dump_svmlight_file(X=X, y=y, query_id=_qid, f=svmlight_path, zero_based=False,
                                verbose=True, parallel=True, append=True, first=bool(i == 0), row_id=row_id, row_id_begin=row_id_begin)
                row_id_begin += y.shape[0]
        else:
            print(f'{svmlight_path} exists, proceeding...')

              
        if qid_mode:
            import subprocess
            import re

            print('Sorting on row_id...')
            # sort --parallel=8 -t ' ' -k 3.5,3n -o data/_20200101_20200102.svmlight.sorted data/_20200101_20200102.svmlight
            n_threads = 8
            cmd = f"sort --parallel={n_threads} -t ' ' -o {svmlight_path}.sorted -k 3.5,3n {svmlight_path} -T ../data_bk/tmp"
            print(cmd)
            subprocess.run(cmd, shell=True, check=True)

            print('resort X_c, X_k np.take, remove leading row_id...')
            row_id_regex = re.compile(r'^(\d+) \d+ qid:', re.MULTILINE)
            sorted_ids = []
            with open(f'{svmlight_path}.sorted', 'r') as sorted_f, open(svmlight_path, 'w') as ori_f:
                for i, line in enumerate(sorted_f):
                    try:
                        row_id = int(row_id_regex.match(line).group(1))
                    except AttributeError:
                        print(f'Row id not found at row {i}')
                        print(f'{line[:50]}')
                    sorted_ids.append(row_id)

                    new_line = re.sub('\d+ ', '', line, count=1)
                    ori_f.write(new_line)
            
            # os.remove(f'{svmlight_path}.sorted')


        print('Dumping X_c, X_k, ...')
        X_c = np.concatenate(X_c)
        X_k = np.concatenate(X_k)

        if qid_mode:
            qid = np.concatenate(qid)

            X_c = X_c.take(sorted_ids)
            X_k = X_k.take(sorted_ids)
            qid = qid.take(sorted_ids)

            np.save(qid_path, qid)

        np.save(X_c_path, X_c)
        np.save(X_k_path, X_k)

    return

if __name__ == "__main__":
    import argparse

    def str2bool(v):
        if isinstance(v, bool):
            return v
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    parser = argparse.ArgumentParser(
        description='Generate within-collection ranking dataset')

    parser.add_argument('--project_id', type=str,
                        default=os.environ['GCLOUD_PROJECT'] if 'GCLOUD_PROJECT' in os.environ else "manga-rock-9cfc9")
    parser.add_argument('--dataset', type=str, default='ml_sample')
    parser.add_argument(
        '--date_begin', type=lambda s: datetime.strptime(s, '%Y%m%d'))
    parser.add_argument(
        '--date_end', type=lambda s: datetime.strptime(s, '%Y%m%d'))
    parser.add_argument('--session_time_out', type=int, default=60)
    parser.add_argument('--user_history_long_date',
                        type=lambda s: datetime.strptime(s, '%Y%m%d'))
    parser.add_argument('--shortterm_history_duration_day',
                        type=int, default=3)
    parser.add_argument('--output_dir', type=str, default='.')
    parser.add_argument('--label', type=str, default='both',
                        choices=['both', 'binary', 'multi'], help='label type to be saved')
    parser.add_argument('--three_labels_mode', type=str2bool,
                        default=False, help='Merge label 2 and 3 to 2')
    parser.add_argument('--single_click_mode', type=str2bool, default=False,
                        help='If a query should contain single or multiple clicks.')
    parser.add_argument('--cache', type=str2bool, default=True,
                        help='Try to look for cache file or save to a cache file if not available.')
    parser.add_argument('--qid_mode', type=str2bool, default=False,
                        help='qid info')

    args = parser.parse_args()

    print(args)

    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    client = bigquery.Client(
        credentials=credentials,
        project=args.project_id
    )
    bqstorageclient = bigquery_storage_v1beta1.BigQueryStorageClient(
        credentials=credentials
    )
    storage_client = storage.Client(
        credentials=credentials,
    )

    print(f'Logged in as {client.get_service_account_email()}')

    ensemble(
        date_begin=args.date_begin,
        date_end=args.date_end,
        shortterm_history_duration_day=args.shortterm_history_duration_day,
        user_history_long_date=args.user_history_long_date,
        session_time_out=args.session_time_out,
        project=args.project_id,
        dataset=args.dataset,
        client=client,
        storage_client=storage_client,
        outdir=args.output_dir,
        bqstorage_client=bqstorageclient,
        label=args.label,
        mode_3_label=args.three_labels_mode,
        single_click_mode=args.single_click_mode,
        cache=args.cache,
        qid_mode=args.qid_mode
    )

    print('Finished')
